# flutter_web3_demo

Demo application for Web3

![Screen Capture](./docs/android_demo.gif) ![Screen Capture](./docs/ios_demo.gif)

I wrote this to get more people using Web3 with Flutter.  This app should provide enough of the
boilerplate configuration and logging so you can quickly prototype or participate in a hackathon etc. 
While there is decent error checking, there are multiple TODO: sections marked in the code which 
call out implementation limitations.  (Demo code != Production code)

## Getting Started

Add your project and Infura API Keys to the build target and run!   Well that is the goal for the class.

### Building 

User specific API keys are not included in the sample code to avoid accidental sharing.
Developer keys are provided via the additional arguments to the build process or set in the 
environment.  This still results in essentially hard coded credentials in the application.

** NOTE: Production applications should take additional precautions to protect keys. **

#### Configuring Android Studio 
Run > Edit Configurations... > Flutter > main.dart > Additional run args:

```bash
--dart-define=WALLETCONNECT_PROJECT_ID=
--dart-define=NODE_API_KEY=
```

See also
 * Get a WalletConnect Project ID from [[https://cloud.walletconnect.com]].  You do not need 
   to fill out the Explorer tab, as the demo app should not be listed in the public repo.
 * The Node API key has been tested with Infura [[https://www.infura.io/networks/ethereum/polygon]]

### Generate ABI wrappers that go in lib/abi
In addition to the JSON parsing code, the web3dart code generates the abi interfaces. From the
project root directory.
```bash
flutter clean
# Grab the third party code
flutter pub get
# Generate the .g files in lib/abi and lib/model
dart run build_runner build --delete-conflicting-outputs
```

## Hard Hat - Generate the Token Contract ABI
To make the example more bottoms up starting from Solidity, the Token page deploys and calls the 
token contract provided by the HardHat Tutorial.  The contract is already in the Flutter repo, but 
for bonus points you can do the generate the ABI yourself.
* Run through the HardHat Tutorial [[https://hardhat.org/tutorial]] and 
* Grab the abi out of the artifacts folder (see  [[https://hardhat.org/hardhat-runner/docs/guides/compile-contracts#compiling-your-contracts ]])
* The web3builders scripts looks for <filename>.abi.json. 
 
## Thirdweb - Download the Split contract ABI
There is a also split contract in the code, but the UI is commented out.  This came from Thirdweb, but I didn't know 
which endpoints to call.  For bonus points you can read more and implement a page for the Split contractL
[[https://thirdweb.com/thirdweb.eth/Split]]
Sources -> ABI 
Copy source and add to project under /lib/abi/thirdweb_split.abi.json

## Supported Platforms
This example is focused on Android and IOS.  The initial setup had additional targets 
configured (i.e. Chrome) but I have not qualified those platforms at this time.

### Disabling a target platform
```bash
flutter config --no-enable-macos-desktop
flutter config --no-enable-linux-desktop
flutter config --no-enable-windows-desktop
```

### Web Platform (Not Supported)
I have made some attempts to keep the Web platform working, but there is a larger image management issue that 
needs to be addressed.  For purposes of the demo app, I have copied some images to the assets/img folder.  
If you add a new blockchain, you will likely need to add the blockchain and currency icons.

Also the WalletConnect Explorer Server currently causes issues with Flutter's web platform.  So you QR is the only 
practical method to connect on that platform at this time.
```
08:42:59 ⛔ ExplorerRegSvc  - WalletConnect Registry Server is not properly configured...
Comment on - https://discord.com/channels/492410046307631105/1073721960288227410
```

## Other Notes
Your app will need to include all the necessary setup (OS etc) for packages like:
* https://pub.dev/packages/url_launcher (To launch and query the OS for wallet apps)
* https://pub.dev/packages/walletconnect_modal_flutter

## Customizations
* lib/consts.dart - chains, methods, etc
* android/app/build.gradle - bundle name 
* ios/Runner/Info-Debug.plist  - bundle name, wallet deep links
* ios/Runner/Info-Release.plist - bundle name, wallet deep links

## Things to try
 - Long press the connect button to test out the alternate launch method for a platform
 - You can get the URL to launch as  QR code (icon on the Connect your wallet) screen
 - Alternatively you can also get the URL copied to the clipboard from the QR screen
 - App level actions (clear all pairings/rerun init) are accessible by sliding the app line.
 - Pairing level actions are accessible by sliding on the wallet entry (ping/disconnect)
 - There are icons to copy the address and show a QR code for an address

## Known issues
The example app is incomplete - just connecting to wallets at the moment.
  - Chrome platform images, I don't have a solution for them yet.
  - The example app only has a single signing client instance.  (no plans for more)
  - While you can initiate a Session and Pairing Ping, the package doesn't lend itself to testing those features.  
     so those who like to see ping counter tx/rx like me, will be underwhelmed.
### TO-DO
  - Writeup how to get the ABI working for your own solidity thing

### Class notes and other information
I turned on the wiki feature of Gitlab, there you can get the latest information and compatibility notes etc.
* https://gitlab.com/graflr/flutter_web3_demo/-/wikis/home