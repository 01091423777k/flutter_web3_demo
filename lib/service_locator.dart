import 'package:flutter_web3_demo/services/explorer_registry_service.dart';
import 'package:flutter_web3_demo/services/navigation_service.dart';
import 'package:flutter_web3_demo/services/walletconnect_service.dart';
import 'package:get_it/get_it.dart';

/// See https://www.filledstacks.com/snippet/dependency-injection-in-flutter/
///
/// Register services defined in the service/ directory here
///  Singleton or Factory
///

/// Usage in a screen etc:
/// import 'package:my_project/service_locator.dart';
/// ...
/// var userService = locator<UserService>();

// GetIt locator = GetIt();  // v1
GetIt locator = GetIt.instance;

Future setupLocator() async {
  // NOTE: Services that are dependent on other services - have to be
  // initialized AFTER their dependencies!

  // Register the NavigationService used for alert dialogs
  locator.registerSingleton<NavigationService>(NavigationService());

  // Register the Wallet Connect Explorer Registry Service
  locator.registerSingleton<ExplorerRegistryService>(ExplorerRegistryService());

  // Register the Wallet Connect Service - (requires analytics service)
  locator.registerSingleton<WalletConnectService>(WalletConnectService());

  //  Wait for all the asyncs to complete
  await locator.allReady();
}
