import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:logger/logger.dart';

// TODO: You never want sensitive API or Crypto private key packaged in your app or source code.
// That said - for this example it is essentially hard coded in the binary (bad)
// Since I don't want to accidentally save a key to revision control I am
// using a command line flag to the flutter build command.
// e.g. --dart-define=INFURA_API_KEY=SOME_VALUE --dart-define=APP_PRIVATE_KEY=OTHER_VALUE
class EnvSettings {
  final logger = Logger(printer: SimpleLogPrinter('EnvSettings '));

  static const appPrivateKey = String.fromEnvironment('APP_PRIVATE_KEY');
  static const web3NodeProviderApiKey = String.fromEnvironment('NODE_API_KEY');
  static const walletConnectProjectId = String.fromEnvironment('WALLETCONNECT_PROJECT_ID');

  bool verifyConfigured() {
    bool result = true;

    // // Check for Application private key
    // if (appPrivateKey.isEmpty) {
    //   logger.w('appPrivateKey is empty.  Please define APP_PRIVATE_KEY from your testing wallet');
    //   logger.w('*** Assuming user will provide signing via WalletConnect instead.***');
    //   result = false;
    // }

    // Check for API key
    if (web3NodeProviderApiKey.isEmpty) {
      logger.e('infuraApiKey is empty.  Please define INFURA_API_KEY provided from https://www.infura.io/');
      result = false;
    }

    // Check for API key
    if (walletConnectProjectId.isEmpty) {
      logger.e(
          'walletConnectProjectId is empty.  Please define WALLETCONNECT_PROJECT_ID provided from https://explorer.walletconnect.com');
      result = false;
    }

    // Common next action for all environment variables.
    if (result == false) {
      logger.i('Please update your build environment to provide the required variables.');
      logger.i('You can provide additional run arguments by editing the Run/Debug configuration');
      logger.i('and supplying a "--dart-define=MISSING_KEY=MISSING_VALUE"');
    }
    return result;
  }
}
