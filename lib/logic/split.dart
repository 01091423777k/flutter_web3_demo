import 'package:flutter_web3_demo/abi/thirdweb_split.g.dart';
import 'package:flutter_web3_demo/consts.dart';
import 'package:flutter_web3_demo/model/blockchain.dart';
import 'package:flutter_web3_demo/model/blockchain_account.dart';
import 'package:flutter_web3_demo/model/env_settings.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:web3dart/web3dart.dart';

/// Split contract class
///
/// See https://thirdweb.com/thirdweb.eth/Split
///
/// This an ETHEREUM focused example with Web3 clients expecting eip155ChainId (int) etc.
/// The Node API Provider was infura, so the endpoints may vary as well if you choose an alternate provider
///

/// TODO: Research https://github.com/simolus3/web3dart/issues/62 to see if contract can be deployed from bytecode

enum ContractStatus {
  uninitialized,
  draft,
  approved,
  deployed,
  burned,
}

class Split {
  bool initDone = false;
  ContractStatus contractStatus = ContractStatus.uninitialized;
  late BlockchainAccount contractAddress;
  late Client httpClient;
  late Web3Client web3Client;
  late String web3NodeProviderUrl;
  late Thirdweb_split splitContract;
  // One we query the contract we can potentially cache these
  EtherAmount? balance;
  List<BlockchainAccount>? payees;

  final logger = Logger(printer: SimpleLogPrinter('SplitContract  '));

  Split({BlockchainAccount? blockchainAccount}) {
    blockchainAccount ??= BlockchainAccount.fromAccountID(accountId: kSplitContractAddress);
    logger.d('Split constructor called with contract address $blockchainAccount');
    init(blockchainAccount);
  }

  init(BlockchainAccount blockchainAccount) {
    contractAddress = blockchainAccount;
    // Web3 Node Provider allows interaction with a node participating in the blockchain via RPC API calls.
    // https://moralis.io/web3-provider-why-blockchain-developers-should-use-one/ Node vs API distinction
    web3NodeProviderUrl =
        '${contractAddress.blockchain.web3NodeProviderEndpoint}/${EnvSettings.web3NodeProviderApiKey}';
    httpClient = Client();
    web3Client = Web3Client(web3NodeProviderUrl, httpClient);
    // Instance to interact with the Split contract using the /lib/abi/thirdweb_split.g  ABI
    splitContract = Thirdweb_split(
        address: contractAddress.toEthereumAddress, client: web3Client, chainId: contractAddress.eip155ChainId);
    initDone = true;
    // Example app assumes contract to be deployed. You can use  https://thirdweb.com/thirdweb.eth/Split
    contractStatus = ContractStatus.deployed;
    logger.i(
        'init - contractAddress ${contractAddress.accountAddress}'); //web3NodeProviderUrl: $web3NodeProviderUrl (redacted)
  }

  // Query how much coin the contract is holding
  Future<BigInt> getBalance() async {
    balance = await web3Client.getBalance(contractAddress.toEthereumAddress);
    logger.d('Contract balance: ${balance?.getInEther ?? 'unknown'} as of block ${await web3Client.getBlockNumber()}');

    await getPayees();
    await getTotalReleased();
    return balance?.getInEther ?? BigInt.zero;
  }

  // Query how much coin the contract is holding
  Future<BigInt> getTotalReleased() async {
    BigInt totalReleased = BigInt.zero;
    if (contractStatus != ContractStatus.deployed || !initDone) {
      return totalReleased;
    }
    totalReleased = await splitContract.totalReleased$2();
    logger.d('getTotalReleased - total released: $totalReleased');
    return totalReleased;
  }

  Future<List<BlockchainAccount>?> getPayees() async {
    if (contractStatus != ContractStatus.deployed || !initDone) {
      return null;
    }
    List<BlockchainAccount> payees = [];
    // https://portal.thirdweb.com/contracts/Split#payeecount
    final int numRecipients = (await splitContract.payeeCount()).toInt();
    for (int i = 0; i < numRecipients; i++) {
      // https://portal.thirdweb.com/contracts/Split#payee
      final EthereumAddress ethereumAddress = await splitContract.payee(BigInt.from(i));
      payees.add(BlockchainAccount(blockchain: Blockchain.polygon, accountAddress: ethereumAddress.hex));
      logger.d('getPayees - payee[$i] : ${payees.last}');
    }
    return payees;
  }

  /// Release funds in the split contract for the credential owner
  ///
  /// [Credentials] compatible with web3dard
  /// [releaseAddress] optional specify a different release address from the credential's address (default)
  Future<String> releaseMatic({required Credentials credentials, EthereumAddress? releaseAddress}) async {
    if (contractStatus != ContractStatus.deployed || !initDone) {
      return "releaseMatic - Error web3Client has not been initialized.";
    }

    EthereumAddress releaseToAddress = releaseAddress ?? credentials.address;
    // final transaction = Transaction.callContract(
    //     contract: splitContract.self, function: splitContract.self.abi.functions[5], parameters: [releaseToAddress]);
    // await web3Client.estimateGas(to: transaction.to, value: transaction.value, data: transaction.data);

    logger.d(
        'releaseMatic - Release the matic for account ${releaseAddress ?? credentials.address} using account ${credentials.address}');
    String result = await splitContract.release(releaseToAddress, credentials: credentials);
    logger.d('releaseMatic - result: $result');

    return result;
  }
}

///
/// Example log trace on Polygon Mainnet
///
// I/flutter (13718): 16:12:57 🐛 SplitContract   - Split constructor called with contract address Polygon 0xD276...754A
// I/flutter (13718): 16:12:57 💡 SplitContract   - init - contractAddress 0xD2762d646Ae16F38A73553dfBb539AC893da754A

/// Wallet Connect session contains wallet on Polygon Mainnet eip155:137
// I/flutter (13718): 16:18:28 🐛 WalletConnctSvc - signEngine.onSessionUpdate  - SessionUpdate(id: 1693171105357155, topic: b9e8402da0040ef4dd8e420ad9b2cb1403ba73a2e8d5ebcb0508bf37fae0b02d, namespaces: {eip155: Namespace(accounts: [eip155:137:0x72A4408DA42DE870499c1841D0e4a49f864e34ba, eip155:NaN:0x72A4408DA42DE870499c1841D0e4a49f864e34ba, eip155:80001:0x72A4408DA42DE870499c1841D0e4a49f864e34ba], methods: [personal_sign, eth_sendTransaction], events: [accountsChanged, chainChanged, connect, disconnect])})

/// Before state checking balances via Infura call
// I/flutter (13718): 16:18:35 🐛 SplitContract   - Contract balance: 0 as of block 46830867
// I/flutter (13718): 16:18:35 🐛 SplitContract   - getPayees - payee[0] : Polygon 0x7fd6...b0b1
// I/flutter (13718): 16:18:35 🐛 SplitContract   - getPayees - payee[1] : Polygon 0x72a4...34ba
// I/flutter (13718): 16:18:35 🐛 SplitContract   - getTotalReleased - total released: 0

/// Wallet Connect paired with Metamask containing keys for account 0x72a4408da42de870499c1841d0e4a49f864e34ba
//I/flutter (13718): 16:18:38 🐛 SplitContract   - releaseMatic - Release the matic for account 0x72a4408da42de870499c1841d0e4a49f864e34ba using account 0x72a4408da42de870499c1841d0e4a49f864e34ba

/// Web3Dart calls the splitContract.release which calls the WalletConnectEip155Credentials - NOTE from is null
// I/flutter (13718): WalletConnectEip155Credentials: sendTransaction - transaction: {from: null, to: 0xd2762d646ae16f38a73553dfbb539ac893da754a, maxGas: null, gasPrice: null, value: null, nonce: null, maxFeePerGas: null, maxPriorityFeePErGas: null}
// I/flutter (13718): transaction.data in bytes: [ 19 16 55 87 00 00 00 00 00 00 00 00 00 00 00 00 72 a4 40 8d a4 2d e8 70 49 9c 18 41 d0 e4 a4 9f 86 4e 34 ba ]

/// WalletConnect forwards the transaction via the method sendTransaction.  Filling in the from: field, but leaving gas etc to the wallet to determine
// I/flutter (13718): WalletConnectEip155Credentials: sendTransaction - chainId: eip155:137, sessionRequestParams: {method: eth_sendTransaction, params: [{from: 0x72a4408da42de870499c1841d0e4a49f864e34ba, to: 0xd2762d646ae16f38a73553dfbb539ac893da754a, data: 1916558700000000000000000000000072a4408da42de870499c1841d0e4a49f864e34ba}]}

/// App launched wallet with this session's  peerMetadata redirect link (no arguments)
// I/flutter (13718): 16:18:38 🐛 WalletConnctSvc - getWalletLaunchUri - for pairingTopic  or sessionTopic b9e8402da0040ef4dd8e420ad9b2cb1403ba73a2e8d5ebcb0508bf37fae0b02d, return universal link: false
// I/flutter (13718): 16:18:38 🐛 HomePage        - AppLifecycleState: AppLifecycleState.inactive.
// I/flutter (13718): 16:18:39 🐛 HomePage        - AppLifecycleState: AppLifecycleState.paused.

/// Wallet responds with transaction ID and returns to demo APP
// I/flutter (13718): 16:21:14 🐛 SplitContract   - releaseMatic - result: 0xb3743ec574acb259918916f983bd6cc660e0abe9793f0da4becc5764d8c7ddc9
// I/flutter (13718): 16:21:14 🐛 HomePage        - AppLifecycleState: AppLifecycleState.resumed.
// I/OpenGLRenderer(13718): Davey! duration=496039ms; Flags=1, FrameTimelineVsyncId=16663680, IntendedVsync=277723269276608, Vsync=277723269276608, InputEventId=0, HandleInputStart=277723269858513, AnimationStart=277723269868148, PerformTraversalsStart=277723269871065, DrawStart=277723270564711, FrameDeadline=277723323276606, FrameInterval=277723269842211, FrameStartTime=16666666, SyncQueued=277723272379972, SyncStart=277723272473878, IssueDrawCommandsStart=277723272767263, SwapBuffers=277723309552736, FrameCompleted=278219308396001, DequeueBufferDuration=4440053, QueueBufferDuration=539323, GpuCompleted=278219308396001, SwapBuffersCompleted=277723310692527, DisplayPresentTime=0,

/// Sanity check of balances post test (Infura calls only)
// I/flutter (13718): 16:21:23 🐛 SplitContract   - Contract balance: 0 as of block 46830947
// I/flutter (13718): 16:21:24 🐛 SplitContract   - getPayees - payee[0] : Polygon 0x7fd6...b0b1
// I/flutter (13718): 16:21:24 🐛 SplitContract   - getPayees - payee[1] : Polygon 0x72a4...34ba
// I/flutter (13718): 16:21:24 🐛 SplitContract   - getTotalReleased - total released: 24680000000000000
