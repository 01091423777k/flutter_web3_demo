import 'dart:convert';
import 'dart:io';

import 'package:eth_sig_util/util/utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter_web3_demo/abi/token.g.dart';
import 'package:flutter_web3_demo/consts.dart';
import 'package:flutter_web3_demo/model/blockchain.dart';
import 'package:flutter_web3_demo/model/blockchain_account.dart';
import 'package:flutter_web3_demo/model/env_settings.dart';
import 'package:flutter_web3_demo/model/wallet_connect_eip155_credential.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:web3dart/web3dart.dart';

// From hard hat tutorial https://hardhat.org/tutorial
// ❯ npx hardhat run scripts/deploy.js --network mumbai
//  Deploying contracts with the account: 0x72A4408DA42DE870499c1841D0e4a49f864e34ba
//  Token address: 0xBd08b78ec1FE51E9B5dB03657c637e8b18379aF1

///  Returns address in EthereumAddress type from web3dart.dart
extension Web3Dart on BlockchainAccount {
  EthereumAddress get ethereumAddress => EthereumAddress.fromHex(accountAddress);
}

class TokenContract {
  bool initDone = false;
  late final BlockchainAccount contractAddress;
  late Client httpClient;
  late Web3Client web3Client;
  late String web3NodeProviderUrl;
  late Token token;
  EtherAmount? balance;

  final logger = Logger(printer: SimpleLogPrinter('TokenContract  '));

  TokenContract({BlockchainAccount? blockchainAccount}) {
    blockchainAccount ??= BlockchainAccount.fromAccountID(accountId: kTokenContractAddress);
    logger.d('Token constructor called with contract address $blockchainAccount');
    init(blockchainAccount);
  }

  init(BlockchainAccount blockchainAccount) {
    contractAddress = blockchainAccount;
    // Web3 Node Provider allows interaction with a node participating in the blockchain via RPC API calls.
    // https://moralis.io/web3-provider-why-blockchain-developers-should-use-one/ Node vs API distinction
    if (EnvSettings.web3NodeProviderApiKey == '') {
      logger.e('init - web3NodeProviderApiKey has not been set.  Unable to initialize contract.');
    }
    web3NodeProviderUrl = '${contractAddress.blockchain.web3NodeProviderEndpoint}';

    if (contractAddress.blockchain != Blockchain.scrollSepolia) {
      // RPC provider does not use the web2NodeProvider API Key
      web3NodeProviderUrl = '$web3NodeProviderUrl/${EnvSettings.web3NodeProviderApiKey}';
    }

    httpClient = Client();
    web3Client = Web3Client(web3NodeProviderUrl, httpClient);

    // Instance to interact with the Split contract using the /lib/abi/token.g.dart
    token = Token(address: contractAddress.ethereumAddress, client: web3Client, chainId: contractAddress.eip155ChainId);
    initDone = true;
    // Example app assumes contract to be deployed. See https://hardhat.org/tutorial/deploying-to-a-live-network
    logger.i(
        'init - contractAddress $contractAddress, web3NodeProviderUrl: ${contractAddress.blockchain.web3NodeProviderEndpoint} (redacted)');
  }

  /// Deploy the bytecode for a EVM compatible contract that requires no arguments.
  /// [name] Contract Name
  /// [credentials] Credential account which determines the blockchain and owner
  // Inspired from
  // https://github.com/xclud/web3dart/issues/110
  // https://github.com/xclud/web3dart/issues/92
  // https://github.com/simolus3/web3dart/issues/62
  Future<BlockchainAccount?> deployContract(
      {required String name, required WalletConnectEip155Credentials credentials}) async {
    final abiStringFile = await rootBundle.loadString('lib/abi/token.abi.json');
    final abiJsonData = jsonDecode(abiStringFile);

    // Note this has no arguments for the constructor see https://github.com/simolus3/web3dart/issues/62
    final bytecode = abiJsonData['bytecode'];

    // Deploying a contract we just need from: (provided by WalletConnect) and the bytecode of the contract.
    final transaction = Transaction(
      to: null,
      data: hexToBytes(bytecode),
    );

    final transactionHash = await web3Client.sendTransaction(
      credentials,
      transaction,
    );
    logger.d(
        'deployContract - Block explorer: ${Blockchain.fromChainId(credentials.chainId)?.blockExplorerTransactionUrl}/$transactionHash');

    TransactionReceipt? receipt;
    for (int i = 1; i < 30; i++) {
      receipt = await web3Client.getTransactionReceipt(transactionHash);
      if (receipt != null) {
        break;
      }
      logger.d('deployContract - Waiting for contract receipt $i');
      sleep(const Duration(seconds: 1));
    }

    if (receipt != null) {
      BlockchainAccount deployAddress = BlockchainAccount(
          blockchain: Blockchain.fromChainId(credentials.chainId)!, accountAddress: receipt.contractAddress!.hex);
      logger
          .d('deployContract - Contract deployed to ${deployAddress.toFullString()}, transaction ID: $transactionHash');
      return deployAddress;
    }
    return null;
  }

  Future<BlockchainAccount> getOwner() async {
    EthereumAddress ownerAddress = await token.owner();
    logger.d('Contract owner: ${ownerAddress.hex}');
    return BlockchainAccount(blockchain: contractAddress.blockchain, accountAddress: ownerAddress.hex);
  }

  /// Query how much coin the is holding
  Future<BigInt> getBalanceOf(BlockchainAccount account) async {
    BigInt balance = await token.balanceOf(account.ethereumAddress);
    logger.d('Balance: $balance for account $account as of block ${await web3Client.getBlockNumber()}');
    return balance;
  }

  /// Call the transfer function of the solidity contract
  Future<String> transfer(
      {required BlockchainAccount to, required BigInt amount, required Credentials credentials}) async {
    logger.d('transfer - to: $to, amount: $amount, from: ${credentials.address.hex}');

    if (!initDone) {
      return "transfer - Error - Web3Client is not initialize.";
    }

    String result = await token.transfer(to.toEthereumAddress, amount, credentials: credentials);
    logger.d('transfer - result: $result');
    return result;
  }
}

/// Example log trace IOS Metamask
///

// flutter: 10:58:09 🐛 TokenContract   - Token constructor called with contract address Mumbai 0xBd08...9aF1
// flutter: 10:58:09 💡 TokenContract   - init - contractAddress Mumbai 0xBd08...9aF1, web3NodeProviderUrl: https://polygon-mumbai.infura.io/v3 (redacted)
//
// flutter: 11:06:19 💡 WalletConnctSvc - onSessionConnect -
// ****Wallet Responded with New Session ****
// *            name: MetaMask Wallet - MetaMask Wallet Integration
// *        accounts: [ eip155:137:0x72A4408DA42DE870499c1841D0e4a49f864e34ba, eip155:80001:0x72A4408DA42DE870499c1841D0e4a49f864e34ba ],
// *         methods: [ personal_sign, eth_sendTransaction ],
// *          events: [ accountsChanged, chainChanged, connect, disconnect ],
// *         expires: 7.0000 days
// *   session topic: 987ce182a2ad35d1899cb2af83f2aff82712a571c3571d5804814de5158279bf,
// *   pairing topic: 0a98bd9400fa3f84833282c3b8161943cbae80b5c60266943db86b833859cd5c,
// *    our redirect: web3_demo_app://,
// *      properties: null,
// *           icons: [],
// flutter: 11:06:30 🐛 WalletConnctSvc - ==Pairings (1)==
// flutter: 11:06:30 🐛 WalletConnctSvc - 29.9998 days topic: 0a98bd9400fa3f84833282c3b8161943cbae80b5c60266943db86b833859cd5c, relay: irn, active: true, peer: MetaMask Wallet, redirect native: metamask://, redirect universal: https://metamask.app.link/

// flutter: 11:07:31 🐛 HomePage        - Get token contract owner button pressed.
// flutter: 11:07:31 🐛 TokenContract   - Contract owner: 0x72a4408da42de870499c1841d0e4a49f864e34ba
// flutter: 11:07:32 🐛 TokenContract   - Balance: 1000000 for account Mumbai 0x72a4...34ba as of block 39495004

/// Transfer button presses
// flutter: 11:08:53 🐛 TokenContract   - Contract owner: 0x72a4408da42de870499c1841d0e4a49f864e34ba
// flutter: 11:08:53 🐛 WalletConnctSvc - getEip155Credentials - blockchainAccount: Mumbai 0x72a4...34ba, sessionTopic: 987ce182a2ad35d1899cb2af83f2aff82712a571c3571d5804814de5158279bf
// flutter: 11:08:53 🐛 TokenContract   - transfer - to: Mumbai 0x7Fd6...b0b1, amount: 10, from: 0x72a4408da42de870499c1841d0e4a49f864e34ba

/// Web3Dart calls the tokenContract.transfer which calls the WalletConnectEip155Credentials - NOTE from is null
// flutter: WalletConnectEip155Credentials: sendTransaction - transaction: {from: null, to: 0xbd08b78ec1fe51e9b5db03657c637e8b18379af1, maxGas: null, gasPrice: null, value: null, nonce: null, maxFeePerGas: null, maxPriorityFeePerGas: null}
// transaction.data in bytes: [ a9 05 9c bb 00 00 00 00 00 00 00 00 00 00 00 00 7f d6 11 af 09 be fb 5f 47 ab b1 9f cc 25 a2 29 97 ae b0 b1 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 0a ]

/// WalletConnect forwards the transaction via the method sendTransaction.  Filling in the from: field, but leaving gas etc to the wallet to determine
// flutter: WalletConnectEip155Credentials: sendTransaction - chainId: eip155:80001, sessionRequestParams: {method: eth_sendTransaction, params: [{from: 0x72a4408da42de870499c1841d0e4a49f864e34ba, to: 0xbd08b78ec1fe51e9b5db03657c637e8b18379af1, data: a9059cbb0000000000000000000000007fd611af09befb5f47abb19fcc25a22997aeb0b1000000000000000000000000000000000000000000000000000000000000000a}]}
// flutter: 11:08:53 🐛 WalletConnctSvc - getWalletLaunchUri - for pairingTopic  or sessionTopic 987ce182a2ad35d1899cb2af83f2aff82712a571c3571d5804814de5158279bf, return universal link: false
// flutter: 11:08:53 🐛 HomePage        - AppLifecycleState: AppLifecycleState.inactive.

/// Reading the balance immediately - there has been no change as the transaction has not been submitted
// flutter: 11:08:53 🐛 TokenContract   - Balance: 1000000 for account Mumbai 0x72a4...34ba as of block 39495041
// flutter: 11:08:53 🐛 TokenContract   - Balance: 0 for account Mumbai 0x7Fd6...b0b1 as of block 39495042

/// After the approval in Metamask we can see the transaction ID returned.
// flutter: 11:10:15 🐛 TokenContract   - transfer - result: 0x44c9d567b359ab306068a2e7e781be6c7ae5989cdc3b160589b8292b26e3d5f5
// https://mumbai.polygonscan.com/tx/0x44c9d567b359ab306068a2e7e781be6c7ae5989cdc3b160589b8292b26e3d5f5

/// Check balance again
// flutter: 11:10:22 🐛 HomePage        - Get token contract owner button pressed.
// flutter: 11:10:23 🐛 TokenContract   - Contract owner: 0x72a4408da42de870499c1841d0e4a49f864e34ba
// flutter: 11:10:23 🐛 TokenContract   - Balance: 999990 for account Mumbai 0x72a4...34ba as of block 39495085
