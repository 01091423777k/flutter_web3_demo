import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

class SessionStatistics {
  DateTime? connectTime;
  DateTime? deleteTime;
  int messageTxCount = 0;
  int messageTxBytes = 0;
  int messageRxCount = 0;
  int messageRxBytes = 0;
  int errorCount = 0;
  int pingTxCount = 0;
  int pingRxCount = 0;
  int expireCount = 0;
  int extendCount = 0;

  SessionStatistics();

  @override
  toString() {
    DateFormat format = DateFormat(DateFormat.HOUR24_MINUTE_SECOND);

    String result =
        'messageCount: $messageTxCount ($messageTxBytes bytes), errorCount $errorCount, pings(TX/RX): $pingTxCount/$pingRxCount, expireCount: $expireCount, extentCount: $extendCount';
    if (connectTime != null) {
      result += ', connectTime: ${format.format(connectTime ?? DateTime.now())}';
    } else {
      result += ', connectTime:  unknown';
    }
    if (deleteTime != null) {
      result += ', deleteTime: ${format.format(deleteTime ?? DateTime.now())}';
    } else {
      result += ', deleteTime:  unknown';
    }
    return result;
  }
}

class WalletConnectServiceStats {
  // Relay Stats
  int relayPairingPingTxCount = 0;
  int relayPairingPingRxCount = 0;
  int relayClientMessageTxCount = 0;
  int relayClientMessageRxCount = 0;
  int relayClientMessageTxBytes = 0;
  int relayClientMessageRxBytes = 0;
  int relayClientTxErrorCount = 0;
  int relayClientRxErrorCount = 0;
  int relayClientReconnectCount = 0;
  DateTime? relayClientConnectTime;

  // Peer Sessions Stats
  // Statistics are per topic
  Map<String, SessionStatistics> sessionsStatsMap = {};

  clearCounters() {
    relayPairingPingTxCount = 0;
    relayPairingPingRxCount = 0;
    relayClientMessageTxCount = 0;
    relayClientMessageRxCount = 0;
    relayClientMessageTxBytes = 0;
    relayClientMessageRxBytes = 0;
    relayClientTxErrorCount = 0;
    relayClientRxErrorCount = 0;
    relayClientReconnectCount = 0;

    // Not clearing the timestamps.
  }

  final logger = Logger(printer: SimpleLogPrinter('SessionTableStat'));

  setSessionConnected(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // New connections will likely need an entry
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.connectTime = DateTime.now();
  }

  setSessionDeleted(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('setSessionDeleted - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.deleteTime = DateTime.now();
  }

  incrementPingTXCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementPingTXCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.pingTxCount++;
  }

  incrementPingRXCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementPingRXCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.pingRxCount++;
  }

  incrementSessionExpireCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementSessionExpireCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.expireCount++;
  }

  incrementSessionExtendCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementSessionExtendCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.extendCount++;
  }

  incrementSessionErrorCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementSessionErrorCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.errorCount++;
  }

  incrementSessionMessageTxCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementSessionMessageTxCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.messageTxCount++;
  }

  incrementSessionMessageRxCount(String topic) {
    if (!sessionsStatsMap.containsKey(topic)) {
      // Add to map but warn, because it should have been there
      logger.w('incrementSessionMessageRxCount - topicStatsMap missing entry for topic $topic');
      sessionsStatsMap[topic] = SessionStatistics();
    }
    sessionsStatsMap[topic]?.messageTxCount++;
  }

  // Print the session stats table
  @override
  String toString() {
    String result = '';
    sessionsStatsMap.forEach((key, value) {
      result += '\nSign Client session topic: $key - $value';
    });
    return (result.isEmpty) ? 'No data.' : result;
  }
}
