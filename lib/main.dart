import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_web3_demo/consts.dart';
import 'package:flutter_web3_demo/logic/split.dart';
import 'package:flutter_web3_demo/logic/token_contract.dart';
import 'package:flutter_web3_demo/model/env_settings.dart';
import 'package:flutter_web3_demo/screens/transfer_screen.dart';
import 'package:flutter_web3_demo/service_locator.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:flutter_web3_demo/services/navigation_service.dart';
import 'package:flutter_web3_demo/services/walletconnect_service.dart';
import 'package:flutter_web3_demo/widgets/blockchain_account.dart';
import 'package:flutter_web3_demo/widgets/connection_metadata_card.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:walletconnect_flutter_v2/walletconnect_flutter_v2.dart';

void main() async {
  Stopwatch stopwatchMain = Stopwatch();
  stopwatchMain.start();
  // Shared Preferences appears to use BinaryMessenger so we need this first
  WidgetsFlutterBinding.ensureInitialized();

  final logger = Logger(printer: SimpleLogPrinter('Main           '));
  logger.d('[${stopwatchMain.elapsedMilliseconds}ms] Logging started');
  // Setup Logging
  Logger.level = Level.verbose;

  logger.d('[${stopwatchMain.elapsedMilliseconds}ms] Verify build contains the required API keys.');
  EnvSettings envSettings = EnvSettings();
  envSettings.verifyConfigured();

  logger.d('[${stopwatchMain.elapsedMilliseconds}ms] Setup locator to find the global instances of our services.');
  try {
    await setupLocator(); // from service_locator.dart
  } catch (error) {
    logger.e('GetIt Locator setup call has failed - $error');
  }

  logger.d('[${stopwatchMain.elapsedMilliseconds}ms] *** Ready run $kAppName ***');
  stopwatchMain.stop();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final navigationService = locator<NavigationService>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Web3 Demo',
      theme: ThemeData(
        useMaterial3: true,
        colorSchemeSeed: Colors.deepOrange,
      ),
      home: const MyHomePage(title: 'ATX DAO - Demo App'),
      navigatorKey: navigationService.navigatorKey, // Supports dialog pop ups from callbacks etc
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  final logger = Logger(printer: SimpleLogPrinter('HomePage       '));
  final walletConnectService = locator<WalletConnectService>();
  // final explorerRegistryService = locator<ExplorerRegistryService>();
  final Split split = Split();
  final TokenContract tokenContract = TokenContract();

  int selectedAccountIndex = 0;

  @override
  void initState() {
    super.initState();

    logger.d('Initialize WalletConnect');
    if (!walletConnectService.initDone) {
      walletConnectService
          .init(
              targetPlatform: (kIsWeb)
                  ? TargetPlatform.fuchsia
                  : (Platform.isAndroid ? TargetPlatform.android : TargetPlatform.iOS))
          .then((_) => _onInitDone());
      // Register observer so we can see app lifecycle changes.
      WidgetsBinding.instance.addObserver(this);
    }
  }

  /// The wallet connect client, sometimes loses the webSocket connection when the app is suspended
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.d("AppLifecycleState: ${state.toString()}.");
    // TODO: Check on the health of the core relay connection and remediate if needed
    // AppLifecycleState.resumed && mounted -> The user is back
  }

  @override
  void dispose() {
    super.dispose();
    // Remove observer for app lifecycle changes.
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Visibility(
              child: Slidable(
            closeOnScroll: false,
            endActionPane: ActionPane(
              motion: const ScrollMotion(),
              extentRatio: 1 / 1.5,
              children: [
                SlidableAction(
                  backgroundColor: Theme.of(context).primaryColorLight,
                  onPressed: (_) async {
                    await walletConnectService.disconnectAllParings();
                    setState(() {});
                  },
                  foregroundColor: Theme.of(context).iconTheme.color,
                  icon: Icons.delete_forever,
                  label: 'Clear\nPairings',
                ),
                SlidableAction(
                  backgroundColor: Theme.of(context).primaryColorDark,
                  onPressed: (_) async {
                    await walletConnectService.init(targetPlatform: Theme.of(context).platform);
                    setState(() {});
                  },
                  //foregroundColor: Theme.of(context).iconTheme.color,
                  icon: Icons.restart_alt,
                  label: 'Rerun\nInit',
                ),
              ],
            ),
            child: ConnectionMetadataCard(
              connectionMetadata: walletConnectService.selfMetadata,
            ),
          )),
          Text(
            'Wallets',
            style: Theme.of(context).textTheme.titleSmall,
          ),
          Flexible(
            flex: 1,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: walletConnectService.activeParingsCount,
              itemBuilder: (context, index) {
                double iconSize = 40.0;
                PairingInfo pairingInfo = walletConnectService.activePairings[index];
                // Icons may be null, or empty lists.  Handle these cases here and later we have an error icon
                List<String> iconList = pairingInfo.peerMetadata?.icons ?? [];
                if (iconList.isEmpty) {
                  logger.w('Incomplete PairingInfo - ${pairingInfo.peerMetadata}');
                  iconList = [''];
                }

                // logger.d('pairingInfo[$index] = ${pairingInfo.toJson()}');
                return Slidable(
                  startActionPane: ActionPane(
                    motion: const ScrollMotion(),
                    children: [
                      SlidableAction(
                        backgroundColor: Theme.of(context).secondaryHeaderColor,
                        onPressed: (_) async {
                          logger.d('Open ${pairingInfo.peerMetadata?.name ?? "Unknown"} wallet.');
                          await launchUrlString(pairingInfo.peerMetadata?.redirect?.native ?? '');
                          setState(() {});
                        },
                        foregroundColor: Theme.of(context).iconTheme.color,
                        icon: FontAwesomeIcons.arrowUpRightFromSquare,
                        label: 'Launch',
                      ),
                      SlidableAction(
                        backgroundColor: Theme.of(context).primaryColorDark, //   .secondaryHeaderColor,
                        onPressed: (_) async {
                          String sessionTopic =
                              walletConnectService.sessionDataFromPairingInfo(pairingInfo)?.topic ?? '';
                          logger.d('Ping Session ${pairingInfo.peerMetadata?.name ?? 'unknown'}');
                          await walletConnectService.pingSession(sessionTopic: sessionTopic);
                          setState(() {});
                        },
                        foregroundColor: Theme.of(context).buttonTheme.colorScheme!.onPrimary,
                        icon: Icons.radar,
                        label: 'Session Ping',
                      ),
                      SlidableAction(
                        backgroundColor: Theme.of(context).primaryColorLight,
                        onPressed: (_) async {
                          logger.d('Pairing ${pairingInfo.peerMetadata?.name ?? 'unknown'}');
                          await walletConnectService.pingPairing(pairingTopic: pairingInfo.topic);
                          setState(() {});
                        },
                        foregroundColor: Theme.of(context).iconTheme.color,
                        icon: Icons.radar,
                        label: 'Pairing Ping',
                      ),
                    ],
                  ),
                  endActionPane: ActionPane(
                    motion: const ScrollMotion(),
                    children: [
                      SlidableAction(
                        backgroundColor: Theme.of(context).primaryColor,
                        onPressed: (_) async {
                          logger.d('Disconnect ${pairingInfo.peerMetadata?.name ?? 'unknown'}');
                          await walletConnectService.disconnectPairing(topic: pairingInfo.topic);
                          setState(() {});
                        },
                        //foregroundColor: Colors.white,
                        icon: Icons.delete_forever,
                        label: 'Disconnect',
                      ),
                    ],
                  ),
                  child: ListTile(
                    visualDensity: VisualDensity.compact,
                    leading: kIsWeb
                        ? null // TODO: Web platform icons for wallets
                        : ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: CachedNetworkImage(
                              imageUrl: iconList[0],
                              height: iconSize,
                              width: iconSize,
                              errorWidget: ((context, url, error) {
                                logger.e(
                                    'ListTile - ${pairingInfo.peerMetadata?.name} has an invalid icon URL in the peerMetadata returned from the pairing session: $url - $error');
                                return FaIcon(
                                  FontAwesomeIcons.circleQuestion,
                                  size: iconSize,
                                );
                              }),
                            ),
                          ),
                    title: Text(
                      pairingInfo.peerMetadata?.name ?? 'unknown',
                      softWrap: false,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Text(
                      pairingInfo.peerMetadata?.description ?? '',
                      softWrap: false,
                      overflow: TextOverflow.ellipsis,
                    ),
                    trailing: Text(WalletConnectService.printDurationFromNow(pairingInfo.expiry)),
                  ),
                );
              },
            ),
          ),
          Text(
            'Accounts',
            style: Theme.of(context).textTheme.titleSmall,
          ),
          Flexible(
            flex: 2,
            child: ListView.builder(
                itemCount: walletConnectService.blockchainAccounts.length,
                itemBuilder: (context, index) {
//                  logger.d('listview: account ${walletConnectService.blockchainAccounts[index].accountAddressShort} domainName:${walletConnectService.blockchainAccounts[index].domainName}');
                  return BlockChainAccountListTile(
                    blockchainAccount: walletConnectService.blockchainAccounts[index],
                    iconSize: 40,
                    selected: index == selectedAccountIndex,
                    onTap: () async {
                      logger.d('User selected account ${walletConnectService.blockchainAccounts[index]}');
                      selectedAccountIndex = index;
                      // String ensName =
                      //     await ensService.getName(walletConnectService.blockchainAccounts[index].accountAddress);
                      // if (ensName.isNotEmpty & mounted) {
                      //   walletConnectService.addAlias(
                      //       blockchainAccount: walletConnectService.blockchainAccounts[index], name: ensName);
                      //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      //     content: Text(
                      //       'ENS Name: $ensName',
                      //       textAlign: TextAlign.center,
                      //     ),
                      //     behavior: SnackBarBehavior.floating,
                      //   ));
                      // }
                      setState(() {});
                    },
                  );
                }),
          ),
          const SizedBox(
            height: 16,
          ),
          const Center(
              child: Text(
            'Long press Connect button for alternate launch method.',
            softWrap: true,
            textAlign: TextAlign.center,
          )),
          // TODO: More of a Future Builder use case, but holding off buttons until wallet connect loads
          Visibility(
            visible: walletConnectService.initDone,
            child: Wrap(
              children: [
                ElevatedButton(
                  onPressed: //!walletConnectService.initDone ? null :
                      () async {
                    await walletConnectService.createWalletConnectSession(context: context, androidUseOsPicker: false);
                    setState(() {});
                  },
                  onLongPress: //!walletConnectService.initDone ? null :
                      () async {
                    await walletConnectService.createWalletConnectSession(context: context, androidUseOsPicker: true);
                    setState(() {});
                  },
                  child: const Text('Connect Wallet'),
                ),
                // ElevatedButton(
                //   onPressed: //!walletConnectService.initDone ? null :
                //       () async {
                //     await walletConnectService.signInWeb3(context: context, androidUseOsPicker: true);
                //     setState(() {});
                //   },
                //   onLongPress: //!walletConnectService.initDone ? null :
                //       () async {
                //     await walletConnectService.signInWeb3(context: context, androidUseOsPicker: false);
                //     setState(() {});
                //   },
                //   child: const Text('Sign in Web3'),
                // ),
                ElevatedButton(
                  onPressed: () {
                    walletConnectService.showRelayInfo();
                    setState(() {});
                  },
                  child: const Text('Show Relay'),
                ),
                ElevatedButton(
                  onPressed: () {
                    walletConnectService.showSignClientInfo();
                    //walletConnectService.showAuthClientInfo();
                    setState(() {});
                  },
                  child: const Text('Show Sessions'),
                ),
                // Because demo code always has a refresh issue...
                // ElevatedButton(
                //   onPressed: () {
                //     split.getBalance();
                //     setState(() {});
                //   },
                //   child: const Text('Split Balance'),
                // ),
                // ElevatedButton(
                //   onPressed: () async {
                //     BlockchainAccount? blockchainAccount =
                //         walletConnectService.blockchainAccounts[selectedAccountIndex];
                //     String sessionTopic =
                //         walletConnectService.getSessionTopicFromAccount(blockchainAccount) ?? 'No Session Topic';
                //     Credentials? credentials = walletConnectService.getEip155Credentials(
                //         sessionTopic: sessionTopic, blockchainAccount: blockchainAccount);
                //     if (credentials == null) {
                //       logger.e('release balance - null credentials.  Bummer.');
                //       return;
                //     }
                //     // call the web3 function, this will queue a send transaction request for the  the wallet
                //     split.releaseMatic(credentials: credentials);
                //     // redirect the user to the wallet associated with this session.
                //     await launchUrlString(walletConnectService.getWalletLaunchString(sessionTopic: sessionTopic) ?? '');
                //   },
                //   child: const Text('Release Balance'),
                // ),

                /// Wait for Metamask to deliver https://github.com/MetaMask/metamask-mobile/issues/6655#issue-1767350452
                // ElevatedButton(
                //   onPressed: () {
                //     BlockchainAccount? blockchainAccount =
                //         walletConnectService.blockchainAccounts[selectedAccountIndex];
                //     String sessionTopic =
                //         walletConnectService.getSessionTopicFromAccount(blockchainAccount) ?? 'No Session Topic';
                //     walletConnectService.walletSwitchEthereumBlockchain(
                //         blockchain: Blockchain.polygon, sessionTopic: sessionTopic);
                //   },
                //   child: const Text('Switch MATIC'),
                // ),
                //
                // ElevatedButton(
                //   onPressed: () {
                //     BlockchainAccount? blockchainAccount =
                //         walletConnectService.blockchainAccounts[selectedAccountIndex];
                //     String sessionTopic =
                //         walletConnectService.getSessionTopicFromAccount(blockchainAccount) ?? 'No Session Topic';
                //     walletConnectService.walletSwitchEthereumBlockchain(
                //         blockchain: Blockchain.ethereum, sessionTopic: sessionTopic);
                //   },
                //   child: const Text('Switch ETH'),
                // ),
                ElevatedButton(
                  onPressed: () async {
                    logger.d('Navigate to Token Test Page.');
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const TransferTokenScreen()),
                    );
                  },
                  child: const Text('Token Page'),
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: const Text('SetState'),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }

  void _onInitDone() {
    logger.i('_onInitDone');
    // re-read walletConnect.initDone in the Visibility widget
    setState(() {});
  }

  // void _onSessionEvent(SessionEvent? args) {
  //   logger.i('Topic: ${args!.topic}\nEvent Name: ${args.name}\nEvent Data: ${args.data}');
  //   setState(() {});
  // }
}
