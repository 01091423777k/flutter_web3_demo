import 'package:flutter/material.dart';
import 'package:flutter_web3_demo/consts.dart';
import 'package:flutter_web3_demo/logic/token_contract.dart';
import 'package:flutter_web3_demo/model/blockchain_account.dart';
import 'package:flutter_web3_demo/model/wallet_connect_eip155_credential.dart';
import 'package:flutter_web3_demo/service_locator.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:flutter_web3_demo/services/walletconnect_service.dart';
import 'package:flutter_web3_demo/widgets/blockchain_account.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:web3dart/web3dart.dart';

class TransferTokenScreen extends StatefulWidget {
  const TransferTokenScreen({super.key});

  @override
  State<TransferTokenScreen> createState() => _TransferTokenScreenState();
}

class _TransferTokenScreenState extends State<TransferTokenScreen> {
  final logger = Logger(printer: SimpleLogPrinter('TransferToken  '));
  final walletConnectService = locator<WalletConnectService>();
  TokenContract tokenContract = TokenContract();
  static const deployedContractsKey = 'deployedContracts';

  BlockchainAccount tokenContractAddress = BlockchainAccount.fromAccountID(accountId: kTokenContractAddress);
  int selectedAccountIndex = 0;
  int selectedContractIndex = 0;
  BlockchainAccount? contractOwner;
  BlockchainAccount toAddress =
      BlockchainAccount.fromAccountID(accountId: 'eip155:80001:0x7Fd611Af09BeFB5f47aBB19FCC25a22997aEb0b1');
  BigInt contractOwnerBalance = BigInt.parse('0');
  BigInt toAddressBalance = BigInt.parse('0');
  List<BlockchainAccount> deployedContracts = [];
  bool initDone = false;

  initContracts() async {
    //logger.d('initContracts');
    // load any deployed contract info
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String>? deployedContractsIds = prefs.getStringList(deployedContractsKey);
    if (deployedContractsIds != null) {
      for (String accountId in deployedContractsIds) {
        try {
          deployedContracts.add(BlockchainAccount.fromAccountID(accountId: accountId));
        } catch (e) {
          logger.w('initContracts - Unable to process accountId: $accountId');
          continue;
        }
        logger.d('initContracts - Restored deployedContracts: $deployedContracts');
      }
    } else {
      logger.w('initContracts - deployedContractsIds is null');
    }
    // Use compile time contract.
    if (deployedContracts.isEmpty) {
      logger.d('initContracts - Initializing deployedContracts with kTokenContractAddress: $kTokenContractAddress');
      deployedContracts = [BlockchainAccount.fromAccountID(accountId: kTokenContractAddress)];
    }
    initDone = true;
    logger.d('initContracts - deployedContracts = $deployedContracts');
  }

  saveDeployedContracts() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    List<String> deployedContractsIds = [];
    for (BlockchainAccount account in deployedContracts) {
      deployedContractsIds.add(account.accountId);
    }
    logger.d('saveDeployedContracts: $deployedContractsIds');
    await prefs.setStringList(deployedContractsKey, deployedContractsIds);
  }

  @override
  void initState() {
    super.initState();
    initContracts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Token Contract'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("Deployed Contracts:"),
          ),
          Flexible(
            flex: 1,
            child: (deployedContracts.isEmpty || !initDone)
                ? Container()
                : ListView.builder(
                    itemCount: deployedContracts.length,
                    itemBuilder: (context, index) {
//                  logger.d('listview: account ${walletConnectService.blockchainAccounts[index].accountAddressShort} domainName:${walletConnectService.blockchainAccounts[index].domainName}');
                      return BlockChainAccountListTile(
                        blockchainAccount: deployedContracts[index],
                        iconSize: 40,
                        selected: index == selectedContractIndex,
                        onTap: () async {
                          logger.d('User selected contract ${deployedContracts[index]}');
                          selectedContractIndex = index;
                          tokenContract = TokenContract(blockchainAccount: deployedContracts[index]);
                          setState(() {});
                        },
                      );
                    }),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("Wallet Accounts:"),
          ),
          Flexible(
            flex: 1,
            child: ListView.builder(
                itemCount: walletConnectService.blockchainAccounts.length,
                itemBuilder: (context, index) {
//                  logger.d('listview: account ${walletConnectService.blockchainAccounts[index].accountAddressShort} domainName:${walletConnectService.blockchainAccounts[index].domainName}');
                  return BlockChainAccountListTile(
                    blockchainAccount: walletConnectService.blockchainAccounts[index],
                    iconSize: 40,
                    selected: index == selectedAccountIndex,
                    onTap: () async {
                      logger.d('User selected account ${walletConnectService.blockchainAccounts[index]}');
                      selectedAccountIndex = index;
                      setState(() {});
                    },
                  );
                }),
          ),
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Token Contract: $tokenContractAddress \n'
                    'Contract Owner: $contractOwner, \n'
                    ' -> balance: $contractOwnerBalance \n'
                    'Transfer To: $toAddress, \n'
                    ' -> balance $toAddressBalance',
                  ),
                ],
              ),
            ),
          ),
          Wrap(
            children: [
              ElevatedButton(
                onPressed: () async {
                  logger.d('Get token contract owner button pressed.');
                  contractOwner = await tokenContract.getOwner();
                  contractOwnerBalance = await tokenContract.getBalanceOf(contractOwner!);
                  // Update the current to address information as well.
                  BlockchainAccount toAddress = BlockchainAccount.fromAccountID(
                      accountId: '${contractOwner!.blockchain.chainId}:0x7Fd611Af09BeFB5f47aBB19FCC25a22997aEb0b1');
                  toAddressBalance = await tokenContract.getBalanceOf(toAddress);
                  //logger.d('Token Contract Owner: $contractOwner, balance: $balance');
                  setState(() {});
                },
                child: const Text('Get Owner'),
              ),
              ElevatedButton(
                onPressed: () async {
                  logger.d('Faucet');
                  launchUrlString(
                      walletConnectService.blockchainAccounts[selectedAccountIndex].blockchain.faucetUrl ?? '');
                  setState(() {});
                },
                child: const Text('Faucet'),
              ),
              ElevatedButton(
                onPressed: () async {
                  contractOwner ??= await tokenContract.getOwner();
                  if (contractOwner != null) {
                    // Destination Bob1 address will match contract owner blockchain
                    BlockchainAccount toAddress = BlockchainAccount.fromAccountID(
                        accountId: '${contractOwner!.blockchain.chainId}:0x7Fd611Af09BeFB5f47aBB19FCC25a22997aEb0b1');
                    String sessionTopic =
                        walletConnectService.getSessionTopicFromAccount(contractOwner!) ?? 'No Session Topic';
                    Credentials? credentials = walletConnectService.getEip155Credentials(
                        sessionTopic: sessionTopic, blockchainAccount: contractOwner!);
                    if (credentials == null) {
                      logger.e('transfer - null credentials.  Bummer.');
                      return;
                    }
                    if (tokenContract.contractAddress.blockchain != contractOwner?.blockchain) {
                      logger.e(
                          'transfer - You are trying to send funds from a contract on ${tokenContract.contractAddress.blockchain} from an account on ${contractOwner?.blockchain}');
                      return;
                    }

                    // call the web3 function, this will queue a send transaction request for the  the wallet
                    Future<String> transactionId =
                        tokenContract.transfer(to: toAddress, amount: BigInt.parse('10'), credentials: credentials);
                    // redirect the user to the wallet associated with this session.
                    await launchUrlString(walletConnectService.getWalletLaunchString(sessionTopic: sessionTopic) ?? '');

                    /// Need to give UI a chance to return. So these values will be
                    logger.d(
                        'Block explorer: ${tokenContract.contractAddress.blockchain.blockExplorerTransactionUrl}/${await transactionId}');
                    contractOwnerBalance = await tokenContract.getBalanceOf(contractOwner!);
                    toAddressBalance = await tokenContract.getBalanceOf(toAddress);
                    logger.d(
                        'Token Contract Owner: $contractOwner, balance: $contractOwnerBalance, to: $toAddress, balance: $toAddressBalance');
                  }
                },
                child: const Text('Owner -> Bob1'),
              ),
              ElevatedButton(
                onPressed: () async {
                  /// Select an account from the UI to be the owner
                  BlockchainAccount owner = walletConnectService.blockchainAccounts[selectedAccountIndex];
                  String sessionTopic = walletConnectService.getSessionTopicFromAccount(owner) ?? 'No Session Topic';
                  WalletConnectEip155Credentials? credentials =
                      walletConnectService.getEip155Credentials(sessionTopic: sessionTopic, blockchainAccount: owner);
                  if (credentials == null) {
                    logger.e('transfer - null credentials.  Bummer.');
                    return;
                  }

                  // Update token class to reflect the blockchain of the account
                  // This will be overwritten
                  tokenContract = TokenContract(blockchainAccount: owner);
                  // call the web3 function, this will queue a send transaction request for the  the wallet
                  Future<BlockchainAccount?> contractAddress =
                      tokenContract.deployContract(name: 'MyTokenContract', credentials: credentials);

                  // redirect the user to the wallet associated with this session.
                  await launchUrlString(walletConnectService.getWalletLaunchString(sessionTopic: sessionTopic) ?? '');

                  BlockchainAccount? deployedContract = await contractAddress;
                  if (deployedContract != null) {
                    deployedContracts.add(deployedContract);
                    tokenContract = TokenContract(blockchainAccount: deployedContract);

                    setState(() {});
                    saveDeployedContracts();
                  }
                },
                child: const Text('Deploy Token'),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {});
                },
                child: const Text('SetState'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
