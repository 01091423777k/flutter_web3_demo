// Global constants / configuration to help making merging easier etc.

import 'package:flutter_web3_demo/model/blockchain.dart';

const String kLogTimeStampFormat = "HH:mm:ss";
const String kSecureStorageTopicKey = "wcTopicString";
const int kRegistryListingQueryCount = 11;
const List<String> kAlwaysExcludeWalletIds = [
  'ecc4036f814562b41a5268adc86270fba1365471402006302e70169465b7ac18', // Zerion
  '8308656f4548bb81b3508afe355cfbb7f0cb6253d1cc7f998080601f838ecee3', // Unstoppable Domains App
//  'afbd95522f4041c71dd4f1a065f971fd32372865b416f95a0b1db759ae33f2a7', // Omni
];

const List<String> kAlwaysIncludeWalletIds = [
  '47bb07617af518642f3413a201ec5859faa63acb1dd175ca95085d35d38afb83', // Keyring Pro
  'c57ca95b47569778a828d19178114f4db188b89b763c899ba0be274e97267d96', // Metamask
//  '4622a2b2d6af1c9844944291e5e7351a6aa24cd7b23099efac1b2fd875da31a0', // Trustwallet
  '1ae92b26df02f0abca6304df07debccd18262fdf5fe82daa81593582dac9a369', // Rainbow
//  'feb6ff1fb426db18110f5a80c7adbde846d0a7e96b2bc53af4b73aaf32552bea', // CosmostationApp
];

//requiredNamespaces: {eip155: {"chains":["eip155:137"],"methods":["eth_signTransaction"],"events":["accountsChanged","chainChanged","connect","disconnect"]}}, optionalNamespaces: null peer name: Keyring

// TODO: Customize for your app - Paring Info
// This is the information we send to wallets to identify our app  - used with the Sign and Auth Clients
const String kAppName = 'ATX DAO Demo App';
const String kAppUrl = 'https://www.atxdao.com/';
const String kAppDescription = 'Making Austin the Crypto Capital of the World.';
const List<String> kAppIcons = [
  'https://firebasestorage.googleapis.com/v0/b/fidograf.appspot.com/o/img%2Fatxdao_logo_square_color.png?alt=media'
];
// Chrome platform leverages local asset files for app logo
const String kAppIconAssetImage = 'assets/img/atxdao_logo_square_color.png';
const String kNativeLink = 'web3_demo_app://';

const String kSplitContractAddress = 'eip155:137:0xD2762d646Ae16F38A73553dfBb539AC893da754A';
// const String kTokenContractAddress = 'eip155:80001:0xBd08b78ec1FE51E9B5dB03657c637e8b18379aF1'; // Mumbai test net
const String kTokenContractAddress = 'eip155:534351:0x62C01db56D30e40Ed526de0DA8c02BAe28B5d266'; // Mumbai test net

// TODO: Customize for your app - Required Namespaces used in SignClient.connect()
// NOTE: Wallets may not like testnets.  Need to research this more
const List<Blockchain> kAppRequiredBlockchains = [Blockchain.ethereum];
const List<Blockchain> kAppOptionalBlockchains = [Blockchain.mumbai, Blockchain.scrollSepolia,Blockchain.polygon];
// NOTE this is an EIP155 specific list Example App doesn't cover non EVM compatible chains
const List<String> kAppRequiredEIP155Methods = [
  // 'eth_sign',
  // 'eth_signTypedData', // Ambiguous suggested to use _v forms
  'personal_sign',
  // 'signTypedData_v4', // Rainbow Android does not support as of 8/25/23
  // 'eth_signTransaction',   // Rainbow Android does not support as of 8/25/23
  'eth_sendTransaction',
  // 'eth_sendRawTransaction',
  // 'wallet_switchEthereumChain',  // Rainbow Android does not support as of 8/25/23
];

const List<String> kAppOptionalEIP155Methods = [
  'signTypedData_v4', // Rainbow Android does not support as of 8/25/23
  'wallet_switchEthereumChain',  // Rainbow Android does not support as of 8/25/23
];

const List<String> kAppRequiredEIP155Events =
    // Blockchain events that your app required direct visibility into the events
    //
    // NOTE: WalletConnect handles most bookeeping with wc_sessionUpdate and wc_sessionExtend
    // https://docs.walletconnect.com/2.0/specs/clients/sign/rpc-methods#wc_sessionupdate
    // https://docs.walletconnect.com/2.0/specs/clients/sign/rpc-methods#wc_sessionextend
    [
  'accountsChanged', // the accounts available to the Provider change
  'chainChanged', // the chain the Provider is connected to changes
  'connect', // the Provider becomes connected
  'disconnect', // the Provider becomes disconnected from all chains
];
const List<String> kAppOptionalEIP155Events =
// Blockchain events that your app required direct visibility into the events
//
// NOTE: WalletConnect handles most bookeeping with wc_sessionUpdate and wc_sessionExtend
// https://docs.walletconnect.com/2.0/specs/clients/sign/rpc-methods#wc_sessionupdate
// https://docs.walletconnect.com/2.0/specs/clients/sign/rpc-methods#wc_sessionextend
[
  'accountsChanged', // the accounts available to the Provider change
  'chainChanged', // the chain the Provider is connected to changes
  'connect', // the Provider becomes connected
  'disconnect', // the Provider becomes disconnected from all chains
];