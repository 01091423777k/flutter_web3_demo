import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:walletconnect_flutter_v2/apis/models/basic_models.dart';

/// Quick widget to display the Peer Client information
class ConnectionMetadataCard extends StatelessWidget {
  const ConnectionMetadataCard({required this.connectionMetadata, Key? key}) : super(key: key);

  final ConnectionMetadata? connectionMetadata;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: SizedBox(
        width: double.infinity,
        child: Card(
          //margin: EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(
                height: 4.0,
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: (kIsWeb)
                    // Browser can't redirect to other sites
                    // TODO: Find a browser friendly way to provide the image
                    ? Image.asset(
                        'assets/img/walletconnect-w-gradient.png', // Placeholder for now
                        height: 60,
                        width: 60,
                      )
                    : CachedNetworkImage(
                        imageUrl:
                            connectionMetadata?.metadata.icons[0] ?? 'https://avatars.githubusercontent.com/u/37784886',
                        height: 60,
                        width: 60,
                  errorWidget: ((context, url, error) {
                    if (kDebugMode) {
                      print('ConnectionMetadataCard - ${connectionMetadata?.metadata.name} has an invalid icon URL: $url, error: $error');
                    }
                    return const FaIcon(FontAwesomeIcons.circleQuestion, size: 60,);}),
                      ),
              ),
              Text(
                connectionMetadata?.metadata.name ?? 'Unknown Name',
                style: Theme.of(context).textTheme.titleSmall,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                connectionMetadata?.metadata.description ?? 'Unknown Description',
                style: Theme.of(context).textTheme.bodyMedium,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                connectionMetadata?.metadata.url ?? 'Unknown URL',
                style: Theme.of(context).textTheme.labelSmall,
                overflow: TextOverflow.ellipsis,
              ),
              // Text(
              //   connectionMetadata?.publicKey ?? 'Unknown public key',
              //   style: Theme.of(context).textTheme.labelSmall,
              //   overflow: TextOverflow.ellipsis,
              // ),
              const SizedBox(
                height: 4.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
