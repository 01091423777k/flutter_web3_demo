import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web3_demo/model/explorer_registry_listing.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// Widget to display and provide a onTap action for a WalletConnect Cloud Registered app.
///
/// [listing]
///
/// TODO: Intelligent way to support larger screens and take advantage of the different icon sizes
/// TODO: 'Recently used' use case or 'Installed' subtitle
class ExplorerRegistryListingIconButton extends StatelessWidget {
  const ExplorerRegistryListingIconButton({
    super.key,
    required this.listing,
    this.onTap,
  });

  final ExplorerRegistryListing listing;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizedBox(
        // Give the button additional margin and tapping area
        width: 80,
        child: Column(
          children: [
            Align(
              alignment: Alignment.center,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: (kIsWeb)
                    // TODO: Find a browser friendly way to provide the image
                    ? Image.asset(
                        'assets/img/walletconnect-w-gradient.png', // Placeholder for now
                        height: 60,
                        width: 60,
                      )
                    : CachedNetworkImage(
                        imageUrl: listing.image_url?.md ?? '',
                        height: 60,
                        width: 60,
                  errorWidget: ((context, url, error) {
                    if (kDebugMode) {
                      print('ExplorerRegistryListingIconButton - ${listing.name} has an invalid md icon URL: $url - $error. \nsm: ${listing.image_url?.sm} \nmd: ${listing.image_url?.md} \nlg: ${listing.image_url?.lg}');
                    }
                    return const FaIcon(FontAwesomeIcons.circleQuestion, size: 60,);}),
                      ),
              ),
            ),
            Text(
              listing.name,
              style: Theme.of(context).textTheme.labelMedium,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
            // 'Recently used' use case or 'Installed' subtitle
            // Text(
            //   listing.additionalDetail,
            //   style: Theme.of(context).textTheme.labelMedium,
            //   textAlign: TextAlign.center,
            //   overflow: TextOverflow.ellipsis,
            // )
          ],
        ),
      ),
    );
  }
}
