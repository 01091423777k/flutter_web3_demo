import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_web3_demo/model/explorer_registry_listing.dart';
import 'package:flutter_web3_demo/widgets/explorer_registry_listing_icon_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';

/// Present a fixed list of wallets for the user to connect using the OS appropriate actions or QR Code
/// Inspired by
/// https://docs.walletconnect.com/2.0/web3modal/about
/// TODO: Does not support searching for wallets.
/// TODO: Does not support dark mode, branding, or color selections
/// TODO: Does not support recently used, or installation status for wallets
/// TODO: Landscape orientation or large screens
/// TODO: Help link
class SimpleWeb3Modal extends StatefulWidget {
  const SimpleWeb3Modal({required this.walletList, required this.urlString, this.showQrCode = false, Key? key})
      : super(key: key);

  final List<ExplorerRegistryListing> walletList;
  final String urlString;
  final bool showQrCode;

  @override
  State<SimpleWeb3Modal> createState() => _SimpleWeb3ModalState();
}

class _SimpleWeb3ModalState extends State<SimpleWeb3Modal> {
  // We maintain state of the showQrCode in the widget
  late bool showQrCode;

  @override
  void initState() {
    super.initState();
    showQrCode = widget.showQrCode;
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    double screenWidth = mediaQueryData.size.width;
    double screenHeight = mediaQueryData.size.width;

    // Ensure the QR doesn't exceed the screen
    double qrSize = min(screenHeight, screenWidth);
    // Cap the qrSize on on non-mobile platforms
    if (Theme.of(context).platform != TargetPlatform.iOS &&
        Theme.of(context).platform != TargetPlatform.android &&
        qrSize > 325) {
      qrSize = 325;
    }

    /// BottomSheet Dialog for users to select their wallet
    /// [wallets] provides the listing data for the wallets to present
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> walletEntries = [];

      // TODO: Support landscape orientation
      // if (MediaQuery.of(context).orientation == Orientation.portrait) {

      for (ExplorerRegistryListing listing in widget.walletList) {
        walletEntries.add(ExplorerRegistryListingIconButton(
          listing: listing,
          onTap: () {
            Navigator.pop(context, listing);
          },
        ));
      }

      // Expensive but works well with bottom sheets
      return IntrinsicHeight(
        child: Container(
          // TODO: Consider AnimatedContainer with changing gradient background?
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.lightBlue, //Theme.of(context).scaffoldBackgroundColor,
          ),
          child: SafeArea(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                  child: Row(
                    children: [
                      Image.asset(
                        'assets/img/walletconnect-logo-white.png',
                        height: 28,
                      ),
                      const Spacer(),
                      IconButton(
                        icon: const FaIcon(
                          FontAwesomeIcons.solidCircleQuestion,
                          size: 28,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          // TODO: Help Screen not implemented.
                        },
                      ),
                      IconButton(
                        icon: const FaIcon(
                          FontAwesomeIcons.solidCircleXmark,
                          size: 28,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context, 'cancelled');
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                    color: Theme.of(context).cardColor, //Colors.white,
                  ),
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: Center(
                              child: Text('Connect your wallet',
                                  style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                      )),
                            ),
                          ),
                          (showQrCode)
                              ? SizedBox(
                                  width: qrSize,
                                  height: qrSize,
                                  child: QrImageView(
                                    version: QrVersions.auto,
                                    data: widget.urlString,
                                    embeddedImage: const AssetImage('assets/img/atxdao_logo_square_color.png'),
                                    size: qrSize,
                                    gapless: false,
                                    embeddedImageStyle: QrEmbeddedImageStyle(
                                      size: Size(qrSize / 5, qrSize / 5),
                                    ),
                                  ),
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: Wrap(
                                      alignment: WrapAlignment.spaceEvenly,
                                      runSpacing: 8,
                                      children: walletEntries,
                                    ),
                                  ),
                                ),
                          // TODO: Add QR Code
                          const SizedBox(
                            height: 16.0,
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 8.0, right: 8.0),
                          child: IconButton(
                            icon: FaIcon(
                              showQrCode ? FontAwesomeIcons.copy : FontAwesomeIcons.qrcode,
                              size: 20,
                              //color: Colors.white,
                            ),
                            onPressed: () async {
                              if (showQrCode) {
                                await Clipboard.setData(ClipboardData(text: widget.urlString));
                                // TODO: Show a snackbar notification, but it's messy with a bottomSheet
                                //
                                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                //   content: Text(
                                //     'URL Copied',
                                //     textAlign: TextAlign.center,
                                //   ),
                                //   behavior: SnackBarBehavior.floating,
                                // ));
                              } else {
                                setState(() {
                                  showQrCode = !showQrCode;
                                });
                              }
                            },
                          ),
                        ),
                      ),
                      Visibility(
                        visible: showQrCode,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0, right: 8.0),
                            child: IconButton(
                              icon: const FaIcon(
                                FontAwesomeIcons.chevronLeft,
                                size: 20,
                                //color: Colors.white,
                              ),
                              onPressed: () async {
                                setState(() {
                                  showQrCode = false;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
