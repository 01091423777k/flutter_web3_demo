import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// Used with the navigation services to pop up an alert dialog, regardless of which screen the user is using.
class DemoAlertDialog extends StatelessWidget {
  const DemoAlertDialog({
    Key? key,
    this.title,
    required this.icon,
    required this.text,
    required this.actions,
    this.iconColor = Colors.red,
  }) : super(key: key);

  final String? title;
  final IconData? icon;
  final Color? iconColor;
  final String text;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      //title: const Text('Confirm Delete'),
      content: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FaIcon(
              icon,
              size: 40.0,
              color: iconColor,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                text,
                style: Theme.of(context).textTheme.titleMedium,
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
      // NOTE: Return values to ShowDialog can be passed via Navigator.pop(context, returnValue)
      actions: actions,
    );
  }
}
