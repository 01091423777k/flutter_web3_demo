import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_web3_demo/model/blockchain.dart';
import 'package:flutter_web3_demo/model/blockchain_account.dart';
import 'package:flutter_web3_demo/services/explorer_registry_service.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class BlockChainAccountListTile extends StatelessWidget {
   BlockChainAccountListTile({
    super.key,
    this.enabled = true,
    this.selected = false,
    this.iconSize,
    this.onTap,
    this.leading,
    this.trailing,
    required this.blockchainAccount,
  });

  final bool enabled;
  final bool selected;
  final double? iconSize;
  final Function()? onTap;
  final Widget? leading;
  final Widget? trailing;
  final BlockchainAccount blockchainAccount;
  bool useAssetImage = kIsWeb;

  @override
  Widget build(BuildContext context) {
    // resource location or URL to icon
    String explorerIconInfo = '';
    if (useAssetImage) {
      explorerIconInfo = ExplorerRegistryService.explorerIconAssetSm(blockchain: blockchainAccount.blockchain) ?? '';
    } else {
      if( blockchainAccount.blockchain == Blockchain.scrollSepolia ){
        explorerIconInfo = 'assets/img/scroll_icon.png';
        useAssetImage = true;
      } else {
        explorerIconInfo = ExplorerRegistryService.explorerIconUrlSm(blockchain: blockchainAccount.blockchain) ?? '';
      }
    }
    return ListTile(
      leading: leading ??
          InkWell(
            onTap: () {
              String explorerUrl = blockchainAccount.explorerUrl;
              if (explorerUrl.isNotEmpty) {
                launchUrl(Uri.parse(explorerUrl));
              }
            },
            child: explorerIconInfo.isNotEmpty
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: (useAssetImage)
                        ? Image.asset(
                            explorerIconInfo,
                            height: iconSize,
                            width: iconSize,
                          )
                        : CachedNetworkImage(
                            imageUrl: explorerIconInfo,
                            errorWidget: ((context, url, error) {
                              if (kDebugMode) {
                                print(
                                    'BlockChainAccountListTile - ${blockchainAccount.blockchain} has an invalid explorer icon URL: $url, error: $error');
                              }
                              return FaIcon(
                                FontAwesomeIcons.circleQuestion,
                                size: iconSize,
                              );
                            }),
                            height: iconSize,
                            width: iconSize,
                          ))
                : Container(),
          ),
      title: Text(
        blockchainAccount.domainName.isNotEmpty ? blockchainAccount.domainName : blockchainAccount.toString(),
        style:
            Theme.of(context).textTheme.titleMedium!.copyWith(color: selected ? Theme.of(context).primaryColor : null),
      ),
      subtitle: Text(
        blockchainAccount.accountAddress,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          trailing ??
              IconButton(
                  icon: const FaIcon(
                    FontAwesomeIcons.copy,
                    size: 20,
                  ),
                  onPressed: () async {
                    Clipboard.setData(ClipboardData(text: blockchainAccount.accountAddress));
                    // TODO: Show a snackbar notification, but it's messy with a bottomSheet
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text(
                        'URL Copied',
                        textAlign: TextAlign.center,
                      ),
                      behavior: SnackBarBehavior.floating,
                    ));
                  }),
          IconButton(
              icon: const FaIcon(
                FontAwesomeIcons.qrcode,
                size: 20,
              ),
              onPressed: () {
                showDialog<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: Padding(
                          // OK I can't center this
                          padding: const EdgeInsets.only(left: 16.0),
                          child: SizedBox(
                            //height: 200,
                            width: 200,
                            child: QrImageView(
                              size: 200,
                              version: QrVersions.auto,
                              data: blockchainAccount.accountAddress,
                            ),
                          ),
                        ),
                      );
                    });
              }),
        ],
      ),
      dense: true,
      enabled: enabled,
      selected: selected,
      selectedTileColor: Theme.of(context).secondaryHeaderColor,
      onTap: onTap,
    );
  }
}
