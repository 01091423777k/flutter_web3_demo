// Copyright (c) 2022 Fifthpint, LLC
//
// GRAFLR™ is a trademark of Fifthpint, LLC and this product includes source
// code developed and copyrighted by, Fifthpint, LLC that may not be used
// without express permission and license. All rights are reserved in source
// code unless otherwise specified. To license our source code, contact
// legal@fifthpint.com.

import 'dart:convert';
import 'dart:io';

import 'package:event/event.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web3_demo/consts.dart';
import 'package:flutter_web3_demo/logic/walletconnect_stats.dart';
import 'package:flutter_web3_demo/model/blockchain.dart';
import 'package:flutter_web3_demo/model/blockchain_account.dart';
import 'package:flutter_web3_demo/model/env_settings.dart';
import 'package:flutter_web3_demo/model/explorer_registry_listing.dart';
import 'package:flutter_web3_demo/model/wallet_connect_eip155_credential.dart';
import 'package:flutter_web3_demo/model/wallet_connect_extensions.dart';
import 'package:flutter_web3_demo/model/wallet_paring.dart';
import 'package:flutter_web3_demo/service_locator.dart';
import 'package:flutter_web3_demo/services/explorer_registry_service.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:flutter_web3_demo/widgets/simple_web3_modal.dart';
import 'package:localstorage/localstorage.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_flutter_v2/walletconnect_flutter_v2.dart';
// import 'package:walletconnect_modal_flutter/services/walletconnect_modal/walletconnect_modal_service.dart';

// https://docs.walletconnect.com/2.0/specs/clients/sign/session-proposal
// = Session Proposal =
// == User Flow ==
// # App displays qrcode or deep link to connect wallet
// # User scans qrcode or redirects to wallet
// # User approves session proposal from App
// # User returns to app after success prompt
// # App receives accounts and namespaces from Wallet
// # App sends request to sign transaction or message
// # User redirects to wallet to inspect request
// # User approves transaction or message request
// # User returns to app after success prompt
// # App receives transaction confirmation or signature
//
// = Proposal Protocol =
// == Prerequisites ==
// Wallet and Dapp are required to establish pairing P before proceeding to Sign protocol execution.
//
// == Protocol ==
// Proposal protocol will be established as follows:
//
// Dapp sends session proposal on pairing P with publicKey, relay, permissions and metadata
// Wallet receives session proposal with required namespaces and public key X on pairing P
// Wallet generates key pair Y
// Wallet settles session with symmetric key derived with keys X and Y
// Session topic B key is derived from sha256 hash of session's symmetric key
// In parallel:
// * Wallet sends settlement payload to topic B with metadata, namespaces and public key Y
// * Wallet sends proposal response on pairing P with publicKey Y
// Dapp receives propose response on pairing P
// Dapp settles session with topic B using derived key
// Dapp receives settlement on topic B
// Dapp sends acknowledgment on topic B

/// Singleton service that interacts with the platform WalletConnect Client Package
///
/// Persists the connection table for connected wallets in secure storage
class WalletConnectService {
  final logger = Logger(printer: SimpleLogPrinter('WalletConnctSvc'));
  final ExplorerRegistryService explorerRegistryService = locator<ExplorerRegistryService>();
  final localStorage = LocalStorage('walletConnect.json');

  WalletConnectServiceStats serviceStats = WalletConnectServiceStats(); // Counters
  late String walletConnectProjectId;
  late TargetPlatform targetPlatform;

  // Keeping the instances private so we can wrap the getters to protect the UI state
  late Web3App _web3App;
  late AuthClient _authClient; // Authorize == Sign in with Web3
  // ModelServices Manages Communications to Explorer Registry
  // late WalletConnectModalService? modalService;
  // Calculated state from callbacks
  bool isSignClientUp = false;

  // Metrics

  // Statistics are per topic
  // Map<String, SessionStatistics> topicStatsMap = {};

  // Misc
  bool initDone = false;
  int sessionNumber = 1;
  int lastSessionUpdateId = 0;
  List<ExplorerRegistryListing> explorerRegistryListings = [];

  // IOS picker result from simpleWeb3Modal
  ExplorerRegistryListing selectedRegistryListing = const ExplorerRegistryListing();

  String _sessionTopic = ''; // used to get information about the session
  /// Internal state cache for the connected accounts.  ENS names can be cached etc.
  List<BlockchainAccount> _blockchainAccounts = [];
  Map<String, WalletPairingInfo> walletPairingInfoMap = {};

  PairingMetadata pairingMetadata = const PairingMetadata(
      url: kAppUrl,
      name: kAppName,
      description: kAppDescription,
      icons: kAppIcons,
      redirect: Redirect(native: kNativeLink));

  List<Blockchain> requiredBlockchains = kAppRequiredBlockchains;
  List<Blockchain> optionalBlockchains = kAppOptionalBlockchains;

  late RequiredNamespace eip155RequiredNamespace;
  late RequiredNamespace eip155OptionalNamespace;

  static final WalletConnectService _singleton = WalletConnectService._internal();

  factory WalletConnectService() {
    return _singleton;
  }

  WalletConnectService._internal() {
    // WalletConnect Project ID from https://cloud.walletconnect.com passed from the environment. See README
    walletConnectProjectId = EnvSettings.walletConnectProjectId;
  }

  ///  Getters and Setters

  int get relayClientMessageCount => serviceStats.relayClientMessageTxCount + serviceStats.relayClientMessageRxCount;

  int get relayClientErrorCount => serviceStats.relayClientTxErrorCount + serviceStats.relayClientRxErrorCount;

  int get relayClientMessageBytes => serviceStats.relayClientMessageTxBytes + serviceStats.relayClientMessageRxBytes;

  Web3App? get web3app => initDone ? _web3App : null;

  /// Returns the first active session that contains the requested account
  String? getSessionTopicFromAccount(BlockchainAccount account) {
    String? sessionTopic;
    ISignEngine signEngine = _web3App.signEngine;

    //logger.d('getSessionTopicFromAccount for account $account');
    for (SessionData? sessionData in signEngine.sessions.getAll()) {
      if (sessionData == null) {
        logger.e('getSessionTopicFromAccount - No active sessions.  Returning null.');
        return null;
      }
      List<BlockchainAccount>? blockchainAccounts = namespacesToBlockchainAccounts(sessionData.namespaces);
      //logger.d('sessionTopic ${sessionData.topic} - accounts: $blockchainAccounts');
      if (blockchainAccounts.contains(account)) {
        // Check to see if this topic is valid
        if (WalletConnectUtils.isExpired(sessionData.expiry)) {
          logger.w('getSessionTopicFromAccount - Found expired match: ${sessionData.topic}.  Ignoring');
          // TODO: Remediate?
        } else {
          //logger.i('Found sessionTopic: $sessionTopic for account $account');
          return sessionData.topic;
        }
      }
    }
    return sessionTopic;
  }

  WalletConnectEip155Credentials? getEip155Credentials(
      {required String sessionTopic, required BlockchainAccount blockchainAccount}) {
    logger.d('getEip155Credentials - blockchainAccount: $blockchainAccount, sessionTopic: $sessionTopic');
    if (!initDone) {
      return null;
    }
    if (!_blockchainAccounts.contains(blockchainAccount)) {
      logger.w(
          'getEip155Credentials - Blockchain account is not in the known cache of accounts.  blockchainAccount: $blockchainAccount, Accounts: $_blockchainAccounts');
      // return null;
    }
    if (!isValidSessionTopic(sessionTopic)) {
      logger.e('getEip155Credentials - Invalid Session Topic: $sessionTopic.  Expired?');
      return null;
    }

    //logger.d('WalletConnectEip155Credentials - Configure Web3Dart client to use WalletConnect for account $blockchainAccount credentials, sessionTopic: $sessionTopic');
    WalletConnectEip155Credentials connectEip155Credentials = WalletConnectEip155Credentials(
        signEngine: _web3App.signEngine,
        sessionTopic: sessionTopic,
        blockchain: blockchainAccount.blockchain,
        credentialAddress: blockchainAccount.toEthereumAddress);
    return connectEip155Credentials;
  }

  /// Returns the URI (native or universal) to launch the wallet
  /// [pairingTopic] The original pairing topic when the wallet first connected
  /// [universal] When true. the universal URI will be returned instead of native
  String? getWalletLaunchString({String? pairingTopic = '', String? sessionTopic, bool universal = false}) {
    logger.d(
        'getWalletLaunchUri - for pairingTopic $pairingTopic or sessionTopic $sessionTopic, return universal link: $universal');

    if (pairingTopic == null && sessionTopic == null) {
      logger.e('getWalletLaunchString - no pairingTopic or sessionTopic provided.   returning null launch string');
      return null;
    }

    if (sessionTopic != null) {
      SessionData? sessionData = _web3App.signEngine.sessions.get(sessionTopic);
      if (sessionData != null) {
        pairingTopic = sessionData.pairingTopic;
      }
    }

    if (pairingTopic != null) {
      PairingInfo? paringInfo = _web3App.signEngine.pairings.get(pairingTopic);

      if (paringInfo != null && paringInfo.active) {
        return universal ? paringInfo.peerMetadata?.redirect?.universal : paringInfo.peerMetadata?.redirect?.native;
      }
    }

    logger.e('getWalletLaunchString - Unable to calculate a valid paringTopic.');
    return null;
  }

  int get proposalSessionCount {
    if (!initDone) {
      return 0;
    }
    return _web3App.signEngine.proposals.getAll().length;
  }

  int get connectedSessionCount {
    if (!initDone) {
      return 0;
    }
    return _web3App.signEngine.sessions.getAll().length;
  }

  int get activeParingsCount {
    if (!initDone) {
      return 0;
    }
    return _web3App.signEngine.pairings.getAll().where((PairingInfo pairingInfo) => pairingInfo.active).length;
  }

  List<PairingInfo> get activePairings {
    if (!initDone) {
      return [];
    }
    return _web3App.signEngine.pairings.getAll().where((PairingInfo pairingInfo) => pairingInfo.active).toList();
  }

  SessionData? sessionDataFromPairingInfo(PairingInfo pairingInfo) {
    if (!initDone) {
      return null;
    }

    Map<String, SessionData> sessions = _web3App.signEngine.getSessionsForPairing(pairingTopic: pairingInfo.topic);
    return (sessions.values.isEmpty) ? null : sessions.values.first;
  }

  /// Returns true if [sessionTopic] is in the list of active sign sessions
  bool isValidSessionTopic(String sessionTopic) {
    // return _web3App.signEngine.sessions.getAll().where((element) => element.topic == sessionTopic).isNotEmpty;
    return _web3App.signEngine.getActiveSessions().keys.contains(sessionTopic);
  }

  //

  bool get isPaired {
    if (!initDone) {
      return false;
    }
    return _web3App.signEngine.getActiveSessions().isNotEmpty;
  }

  String get relayUrl {
    if (!initDone) {
      return 'Not configured';
    }
    return _web3App.signEngine.core.relayUrl;
  }

  bool get isRelayUp {
    if (!initDone) {
      return false;
    }
    return _web3App.core.relayClient.isConnected;
  }

  ConnectionMetadata? get selfMetadata => ConnectionMetadata(publicKey: 'N/A', metadata: pairingMetadata);

  ConnectionMetadata? get peerMetadata {
    if (isPaired) {
      return _web3App.signEngine.sessions.get(_sessionTopic)?.peer;
    }
    return null;
  }

  /// Stats Getter for the last active connection
  int get sessionPingCount {
    if (serviceStats.sessionsStatsMap.containsKey(_sessionTopic)) {
      return serviceStats.sessionsStatsMap[_sessionTopic]?.pingTxCount ?? 0;
    }
    return 0;
  }

  /// Stats Getter for the last active connection
  int get sessionErrorCount {
    if (serviceStats.sessionsStatsMap.containsKey(_sessionTopic)) {
      return serviceStats.sessionsStatsMap[_sessionTopic]?.errorCount ?? 0;
    }
    return 0;
  }

  /// Stats Getter for the last active connection
  int get sessionExtendCount {
    if (serviceStats.sessionsStatsMap.containsKey(_sessionTopic)) {
      return serviceStats.sessionsStatsMap[_sessionTopic]?.extendCount ?? 0;
    }
    return 0;
  }

  /// Stats Getter for the last active connection
  int get sessionExpireCount {
    if (serviceStats.sessionsStatsMap.containsKey(_sessionTopic)) {
      return serviceStats.sessionsStatsMap[_sessionTopic]?.expireCount ?? 0;
    }
    return 0;
  }

  String get walletName {
    String walletName = 'No wallet available';
    if (isPaired) {
      walletName = _web3App.signEngine.sessions.get(_sessionTopic)?.peer.metadata.name ?? walletName;
    }
    return walletName;
  }

  String get walletDescription {
    String description = 'No description available.';
    if (isPaired) {
      description = _web3App.signEngine.sessions.get(_sessionTopic)?.peer.metadata.description ?? description;
    }
    return description;
  }

  String get walletUrl {
    String url = 'No Url available.';
    if (isPaired) {
      url = _web3App.signEngine.sessions.get(_sessionTopic)?.peer.metadata.url ?? url;
    }
    return url;
  }

  //"namespaces":{"eip155":{"accounts":["eip155:137:0x7Fd611Af09BeFB5f47aBB19FCC25a22997aEb0b1"],"methods":["eth_sign","personal_sign","signTypedData_v4"],"events":["accountsChanged","chainChanged","connect","disconnect"]}}
  int get walletChainId {
    int chainId = -1;
    if (isPaired) {
      //chainId = walletConnect.session.chainId;
    }
    return chainId;
  }

  /// Returns a list of CAIP-25 accountId Strings for all paired wallets
  List<BlockchainAccount> get blockchainAccounts {
    if (!initDone) {
      return [];
    }

    return _blockchainAccounts;
  }

  void addAlias({required BlockchainAccount blockchainAccount, required String name}) {
    for (int i = 0; i < _blockchainAccounts.length; i++) {
      if (_blockchainAccounts[i].accountAddress == blockchainAccount.accountAddress) {
        // Update our internal cache listing entry with the address name
        _blockchainAccounts[i].addAlias(name);
        // Address may appear in multiple wallets, so continue to searching.
      }
    }

    // for (int i =0 ; i < _blockchainAccounts.length; i++){
    //   logger.i('${_blockchainAccounts[i]} ${_blockchainAccounts[i].domainName}, ${_blockchainAccounts[i].domainNames}');
    //   }
  }

  // May want to check if isKnownSessionTopic && not expired
  bool isKnownSessionTopic(String sessionTopic) {
    if (!initDone) {
      return false;
    }

    for (SessionData sessionData in _web3App.signEngine.sessions.getAll()) {
      if (sessionData.topic == sessionTopic) {
        logger.d('isKnownSession - ${sessionData.peer.metadata.name} matched a known session topic: $sessionTopic}');
        // Any match return true
        return true;
      }
    }
    return false;
  }

  Future<void> savePairingInfo() {
    return localStorage.setItem('WalletInfo', jsonEncode(walletPairingInfoMap));
  }

  Map<String, WalletPairingInfo> restorePairingInfo() {
    Map<String, WalletPairingInfo> result = {};
    var storageWalletInfo = localStorage.getItem('WalletInfo');
    if (storageWalletInfo == null) {
      logger.i('No previous pairing sessions restored.');
      return result;
    } else {
      try {
        var decoded = jsonDecode(storageWalletInfo);
        result = Map.castFrom(decoded); // Learned something new
        logger.d('Restored ${result.length} pairing sessions from storage.');
      } catch (e) {
        logger.e('Failed to jsonDecode storage value: $storageWalletInfo. \$e');
        return result;
      }
    }
    return result;
  }

  ///  Initialise WalletConnect Service
  ///
  Future<void> init({
    required TargetPlatform targetPlatform,
  }) async {
    // Wallet Connect Session Storage - So we can persist connections
    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();
    // Save this since we check it often and context isn't always handy
    this.targetPlatform = targetPlatform;

    eip155RequiredNamespace = RequiredNamespace(
      chains: supportedBlockchainsToCAIP2List(namespace: Blockchain.ethereum.namespace),
      methods: kAppRequiredEIP155Methods,
      events: kAppRequiredEIP155Events,
    );

    eip155OptionalNamespace = RequiredNamespace(
      chains: supportedBlockchainsToCAIP2List(namespace: Blockchain.ethereum.namespace),
      methods: kAppOptionalEIP155Methods,
      events: kAppOptionalEIP155Events,
    );

    // Restore State
    await localStorage.ready;
    restorePairingInfo();

    int startingMilliseconds = stopwatch.elapsedMilliseconds;
    // Try to catch the simpler errors earlier.
    if (!kIsWeb) {
      logger.d('[${stopwatch.elapsedMilliseconds}ms] Sanity check network connectivity.');
      try {
        final result = await InternetAddress.lookup('relay.walletconnect.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          logger.d(
              '[${stopwatch.elapsedMilliseconds}ms] relay.walletconnect.com DNS resolved to ${result.length} addresses in ${stopwatch.elapsedMilliseconds - startingMilliseconds}ms.');
        }
      } on SocketException catch (_) {
        logger.e(
            '[${stopwatch.elapsedMilliseconds}ms]  Failed DNS lookup of relay.walletconnect.com.  Internet connectivity is required!');
      }
    }

    /// I can't believe there are no counters - only the "now" state.
    serviceStats.clearCounters();

    logger.d('[${stopwatch.elapsedMilliseconds}ms] initWeb3App.');
    try {
      await initWeb3App();
    } on WalletConnectError catch (e, s) {
      logger
          .e('[${stopwatch.elapsedMilliseconds}ms] initWeb3App threw WalletConnectError ${e.code} : ${e.message}\n$s');
    }

    /// Pull Registry Information
    logger.d('[${stopwatch.elapsedMilliseconds}ms] query registry for compatible wallets.');

    if (kAlwaysIncludeWalletIds.isEmpty) {
      explorerRegistryListings = [];
    } else {
      // Start with the Always Include Ids - fetch to get the latest registry data
      explorerRegistryListings = await explorerRegistryService.readWalletRegistry(
        limit: kAlwaysIncludeWalletIds.length,
        targetPlatform: targetPlatform,
        alwaysIncludeWalletIds: kAlwaysIncludeWalletIds,
      );
    }
    // Get additional listings that match our criteria
    final List<ExplorerRegistryListing> additionalListings = await explorerRegistryService.readWalletRegistry(
      limit: kRegistryListingQueryCount,
      supportedCAIP2Chains: requiredBlockchains.join(','),
      targetPlatform: targetPlatform,
      alwaysExcludeWalletIds: kAlwaysExcludeWalletIds,
    );

    // Add listings, but ignore kAlwaysIncludeWalletIds to avoid duplicates
    // Fancy dart de-duplication would be better here - or have explorerRegistryService.readWalletRegistry
    // do the query with a single call
    for (ExplorerRegistryListing listing in additionalListings) {
      if (!kAlwaysIncludeWalletIds.contains(listing.id)) {
        explorerRegistryListings.add(listing);
      }
    }

    // logger.d('[${stopwatch.elapsedMilliseconds}ms] Instantiate WalletConnectModalService.');
    // modalService = WalletConnectModalService(
    //   projectId: walletConnectProjectId,
    //   metadata: pairingMetadata,
    //   web3App: _web3App,
    //   recommendedWalletIds: Set.from(kAlwaysIncludeWalletIds),
    //   excludedWalletIds: Set.from(kAlwaysExcludeWalletIds),
    //   excludedWalletState: ExcludedWalletState.list, // TODO: Have no idea what this default means
    // );
    // await modalService!.init();

    initDone = true;
    logger.d('[${stopwatch.elapsedMilliseconds}ms] init complete.');
  }

  /// Core /  Relay
  ///

  /// Set up the connection between this core client and the Core relay node
  Future relayClientConnect({ICore? core, String? relayUrl}) async {
    core = _web3App.signEngine.core;
    relayUrl = relayUrl ?? core.relayUrl;
    logger.d('relayClientConnect - protocol ${core.protocol} v${core.version} url: $relayUrl');
    core.relayClient.connect(relayUrl: relayUrl);
    return;
  }

  /// Tear down the connection between this core client and the Core relay node
  Future relayClientDisconnect({ICore? core}) async {
    core = _web3App.signEngine.core;
    logger.d('relayClientDisconnect - ');
    core.relayClient.disconnect();
    return;
  }

  /// One way to spot check flapping connections
  String get relayClientUptime {
    return (serviceStats.relayClientConnectTime == null)
        ? 'unknown duration'
        : '${DateTime.now().difference(serviceStats.relayClientConnectTime!).inSeconds} seconds';
  }

  /// Converter method to get from the list of Blockchains to a string of CAIP2 chainIds
  String get supportedBlockchainsToCAIP2String {
    // String supportedChainIds = supportedBlockchains.fold("", (String result, Blockchain blockchain) {
    //   return "$result${blockchain.namespace}:${blockchain.reference},";
    // });
    // if (supportedChainIds.length > 1) {
    //   // Chop trailing comma
    //   supportedChainIds = supportedChainIds.substring(0, supportedChainIds.length - 1);
    // }

    String chainIds = '';
    if (requiredBlockchains.isNotEmpty) {
      for (Blockchain blockchain in requiredBlockchains) {
        // Comma separate after first entry
        chainIds = chainIds + (chainIds == '' ? '' : ',') + blockchain.chainId;
      }
    }
    // logger.i('supportedBlockchainsToCAIP2String - supportedChainIds: $supportedChainIds  chainIds:$chainIds');
    return chainIds;
  }

  /// Converter method to get from the list of Blockchains to a string of CAIP2 chainIds
  List<String> supportedBlockchainsToCAIP2List({required String namespace}) {
    List<String> caip2Strings = [];
    if (requiredBlockchains.isNotEmpty) {
      for (Blockchain blockchain in requiredBlockchains) {
        if (blockchain.namespace == namespace) {
          caip2Strings.add(blockchain.chainId);
        }
      }
    }
    return caip2Strings;
  }

  /// Hook into as many events as we can find
  ///
  Future<bool> registerRelayListeners(ICore core) async {
    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();

    // logger.d('registerRelayListeners');

    // RELAY CONNECT
    core.relayClient.onRelayClientConnect.subscribe((EventArgs? eventArgs) {
      // Mark relay client connection up
      logger.d('Core.onRelayClientConnect - isRelayUp: $isRelayUp, last uptime:  $relayClientUptime');
      serviceStats.relayClientConnectTime = DateTime.now();
      if (eventArgs != null) {
        logger.w(
            'Core.onRelayClientConnect  - Unexpected event arguments. isRelayUp: $isRelayUp, eventArgs: $eventArgs, ');
        return;
      }
      // TODO: Check if we already have a relay connection to the same peer and take corrective action
      // https://github.com/orgs/WalletConnect/discussions/2410
    });

    // RELAY DISCONNECT
    core.relayClient.onRelayClientDisconnect.subscribe((EventArgs? eventArgs) {
      logger.d('Core.onRelayClientDisconnect - isRelayUp: $isRelayUp,  Connection lasted $relayClientUptime.');
      serviceStats.relayClientConnectTime = null;

      // package will attempt automatically so just log.
      // see: https://github.com/WalletConnect/WalletConnectFlutterV2/issues/31

      if (eventArgs != null) {
        logger.d(
            'Core.onRelayClientDisconnect  - Unexpected event arguments. isRelayUp: $isRelayUp, eventArgs: $eventArgs, ');
        return;
      }
    });

    // RELAY ERROR
    // I/flutter (22061): 🐛  WalletConnctSvc - Core.onRelayClientError  - error: Bad state: The client closed with pending request "irn_publish".
    core.relayClient.onRelayClientError.subscribe((ErrorEvent? errorEvent) {
      serviceStats.relayClientTxErrorCount++; // TODO: Identify direction?? TX/RX
      if (errorEvent != null) {
        logger.e('Core.onRelayClientError  - error: ${errorEvent.error}');

        // Bad state: The client is closed.
        if (errorEvent.error.toString().contains('client is closed')) {
          if (isRelayUp) {
            logger.e('Core.onRelayClientError  - Relay is down, but we thought it was up!  Missing logic path!');
          }
        }

        return;
      }
      logger.e('Core.onRelayClientError - null data passed');
    });

    // RELAY MESSAGE
    core.relayClient.onRelayClientMessage.subscribe((MessageEvent? event) {
      if (event != null) {
        String decodedMessage = utf8.decode(base64Decode(event.message));
        logger.d(
            'Core.onRelayClientMessage  - topic: ${event.topic} message: $decodedMessage [${event.message.length} bytes]');
        serviceStats.relayClientMessageTxCount++;
        serviceStats.relayClientMessageTxBytes += event.message.length;
        // If ping is successful, topic is echo'd in listener.

        logger.i('${event.message} \n$decodedMessage');
        if (event.topic == decodedMessage) {
          logger.i('Ping Success for topic ${event.topic}.');
          serviceStats.relayPairingPingRxCount++;
        }
        return;
      }
      logger.d('Core.onRelayClientMessage - null event data passed');
    });

    // RELAY SUBSCRIPTION CREATED
    core.relayClient.onSubscriptionCreated.subscribe((SubscriptionEvent? event) {
      if (event != null) {
        logger.d('Core.onSubscriptionCreated  - id: ${event.id}');
        return;
      }
      logger.d('Core.onSubscriptionCreated - null data passed');
    });

    // RELAY SUBSCRIPTION DELETED
    core.relayClient.onSubscriptionDeleted.subscribe((SubscriptionDeletionEvent? event) {
      if (event != null) {
        logger.d('Core.onSubscriptionDeleted  - id: ${event.id}, reason: ${event.reason}');
        return;
      }
      logger.d('Core.onSubscriptionDeleted - null data passed');
    });

    // RELAY SUBSCRIPTION RESUBSCRIBED
    core.relayClient.onSubscriptionResubscribed.subscribe((EventArgs? eventArgs) {
      if (eventArgs != null) {
        logger.d('Core.onSubscriptionResubscribed  - args: $eventArgs');
        return;
      }
      logger.d('Core.onSubscriptionResubscribed - null data passed');
    });

    // RELAY SUBSCRIPTION SYNC
    core.relayClient.onSubscriptionSync.subscribe((EventArgs? eventArgs) {
      if (eventArgs != null) {
        logger.d('Core.onSubscriptionSync  - args: $eventArgs');
        return;
      }
      logger.d('Core.onSubscriptionSync - null data passed');
    });

    /// Pairing Events
    core.pairing.onPairingPing.subscribe((PairingEvent? event) {
      serviceStats.relayPairingPingRxCount++;
      if (event != null) {
        logger.d(
            'pairing.onPairingPing  - id: ${event.id} pairing topic: ${event.topic}, error ${event.error?.message ?? 'N/A'}');
        return;
      }
      logger.d('pairing.onPairingPing - null data passed');
    });

    logger.d('registerRelayListeners - Completed in ${stopwatch.elapsedMilliseconds}ms.');
    return true;
  }

  void showRelayInfo({ICore? core}) {
    core = core ?? _web3App.signEngine.core;
    logger.d(
        '=Core Services ${core.protocol}: v${core.version}, relayUrl: ${core.relayUrl}, pushUrl: ${core.pushUrl}, uptime: $relayClientUptime=');

    // Pairings
    List<PairingInfo> pairings = core.pairing.getPairings();

    logger.d('==Pairings (${pairings.length})==');
    if (pairings.isEmpty) {
      logger.d('No Pairings.');
    } else {
      for (PairingInfo pairingInfo in pairings) {
        logger.d(
            '${printDurationFromNow(pairingInfo.expiry)} topic: ${pairingInfo.topic}, relay: ${pairingInfo.relay.prettyPrint}, active: ${pairingInfo.active}, peer: ${pairingInfo.peerMetadata?.name ?? 'null'}, redirect native: ${pairingInfo.peerMetadata?.redirect?.native ?? 'null'}, redirect universal: ${pairingInfo.peerMetadata?.redirect?.universal ?? 'null'}');
      }
    }
  }

  /// Used to evaluate if core is currently online. Timeout at 30 seconds
  /// https://docs.walletconnect.com/2.0/specs/clients/core/pairing/rpc-methods#wc_pairingping
  Future pingPairing({ICore? core, required String pairingTopic}) async {
    core = core ?? _web3App.signEngine.core;
    // Verify pairing topic exists - easier way?
    if (core.pairing.getPairings().where((e) => e.topic == pairingTopic).isEmpty) {
      logger.e('pingPairing - topic not found, topic:$_sessionTopic');
      serviceStats.relayClientTxErrorCount++;
      return;
    }

    logger.d('pingPairing - pairing topic: $pairingTopic');
    core.pairing.ping(topic: pairingTopic);
    serviceStats.relayPairingPingTxCount++;
  }

  /// For a signing session - ping to determine if the app is online
  Future pingSession({ICore? core, required String sessionTopic}) async {
    core = core ?? _web3App.signEngine.core;
    // Verify session topic exists - easier way?
    if (!isValidSessionTopic(sessionTopic)) {
      logger.e('pingSessionTopic - Invalid session topic: $sessionTopic');
      serviceStats.relayClientTxErrorCount;
      return;
    }

    logger.d('pingSession - topic: $sessionTopic');
    SessionData? sessionData = _web3App.signEngine.sessions.get(sessionTopic);

    if (sessionData == null) {
      logger.e('pingSession - Null data returned for topic: $sessionTopic');
      return;
    }

    Duration duration = DateTime.fromMillisecondsSinceEpoch(sessionData.expiry * 1000).difference(DateTime.now());
    if (duration.isNegative) {
      logger.w('pingSession - Ping on expired session topic ${sessionData.topic}. Ignored');
    } else {
      serviceStats.incrementPingTXCount(sessionData.topic);
      await _web3App.signEngine.ping(topic: sessionData.topic);
    }
  }

  /// Tells the relay we are no longer interested in the topic.  Clear out local tracking storage etc..
  ///
  /// Used to inform the peer to close and delete a pairing.
  /// The associated authentication state of the given pairing must also be deleted.
  /// https://docs.walletconnect.com/2.0/specs/clients/core/pairing/rpc-methods#wc_pairingdelete
  Future disconnectPairing({ICore? core, required String topic}) async {
    logger.d('disconnectPairing - ');
    core = core ?? _web3App.signEngine.core;
    // Verify topic exists - easier way?
    if (core.pairing.getPairings().where((e) => e.topic == topic).isEmpty) {
      logger.e('disconnectPairing - topic not found, topic:$_sessionTopic');
      serviceStats.incrementSessionErrorCount(topic);
      return;
    }
    try {
      await core.pairing.disconnect(topic: topic);
    } on WalletConnectError catch (e, s) {
      if (e.code == 8) {
        logger.w(
            'disconnectPairing - WalletConnectError ${e.code} : ${e.message},  Expired pairing or Never paired topic?');
      } else {
        logger.e('disconnectPairing - WalletConnectError ${e.code} : ${e.message}\n$s');
      }
    } catch (e, s) {
      logger.e('disconnectPairing - Unexpected error: $e, topic:$_sessionTopic\n$s');
    }
    return;
  }

  /// For all core pairings, disconnect the session and clear out the stored session data
  Future disconnectAllParings({ICore? core}) async {
    logger.d('disconnectAllParings - ');
    core = core ?? _web3App.signEngine.core;
    // Parings
    List<PairingInfo> parings = core.pairing.getPairings();

    logger.d('==Pairings (${parings.length})==');
    if (parings.isEmpty) {
      logger.d('No Pairings.');
    } else {
      for (PairingInfo pairingInfo in parings) {
        logger.d(
            'disconnectAllParings - ${printDurationFromNow(pairingInfo.expiry)} topic: ${pairingInfo.topic}, relay: ${pairingInfo.relay}, active: ${pairingInfo.active}, ${pairingInfo.peerMetadata?.name ?? 'unavailable'}');
        try {
          await core.pairing.disconnect(topic: pairingInfo.topic);
        } on WalletConnectError catch (e, s) {
          if (e.code == 6) {
            logger.w(
                'disconnectAllParings - WalletConnectError ${e.code} : ${e.message},  Unsuccessful pairing attempt?');
          } else {
            logger.e('disconnectAllParings - WalletConnectError ${e.code} : ${e.message}\n$s');
          }
        } catch (e, s) {
          logger.e('disconnectAllParings - Unexpected error: $e, topic ${pairingInfo.topic}\n$s');
        }
      }
    }
    return;
  }

  ///  Print human readable string duration given a future Epoch time
  /// [secondsSinceEpoch]
  static String printDurationFromNow(int secondsSinceEpoch) {
    Duration duration = DateTime.fromMillisecondsSinceEpoch(secondsSinceEpoch * 1000).difference(DateTime.now());
    if (duration.isNegative) {
      return '<expired>';
    }
    if (duration.inDays > 0) {
      return "${(duration.inMicroseconds / Duration.microsecondsPerDay).toStringAsFixed(4).padLeft(4)} days";
    }
    if (duration.inHours > 1) {
      return "${duration.inHours}:${duration.inMinutes.remainder(60).toString().padLeft(2, '0')}:${(duration.inSeconds.remainder(60))}";
    }
    return "    ${duration.inMinutes.remainder(60).toString().padLeft(2, ' ')}:${(duration.inSeconds.remainder(60).toString().padLeft(2, '0'))}";
  }

  void showSessionData(SessionData sessionData) {
    logger.d(
        '${printDurationFromNow(sessionData.expiry)} topic: ${sessionData.topic}, pairing topic: ${sessionData.pairingTopic} ack: ${sessionData.acknowledged}, \npeer name: ${sessionData.peer.metadata.name} accounts: ${printNameSpacesAccounts(sessionData.namespaces)} sessionProperties: ${sessionData.sessionProperties}');
  }

  /// Sign Client Sessions
  ///
  Future initWeb3App({
    String walletConnectProjectId = EnvSettings.walletConnectProjectId,
  }) async {
    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();

    logger.d('initWeb3App - Initializing Web3App Instance');
    try {
      _web3App = await Web3App.createInstance(
        projectId: walletConnectProjectId,
        metadata: pairingMetadata,
        // See definition at the top of the class
        memoryStore: false,
        // Persist session state in secure like storage
        relayUrl: WalletConnectConstants.DEFAULT_RELAY_URL,
        logLevel: Level.nothing, // Level.verbose,
      );
    } on WalletConnectError catch (e, s) {
      if (e.code == 401) {
        logger.e(
            'initWeb3App - Project id does not have access to relay services.  \nwalletConnectProjectId: $walletConnectProjectId, error:${e.message}');
      } else {
        logger.e('initWeb3App -  WalletConnectError: ${e.code} - ${e.message}\n$s');
      }
    } on JsonRpcError catch (e, s) {
      logger.e('initWeb3App  - JsonRpcError: ${e.code} - ${e.message}\n$s');
    } catch (e, s) {
      logger.e('initWeb3App -  Exception: $e\n$s');
    }
    await registerRelayListeners(_web3App.signEngine.core);
    await registerSignEngineListeners(signEngine: _web3App.signEngine);

    // Core relay client configuration
    // The core will likely have connected to the relay before the onConnect callback is registered.
    serviceStats.relayClientConnectTime = DateTime.now();

    // Read the connected accounts
    initBlockchainAccounts();

    //Flag completion
    stopwatch.stop();
    logger.d('initWeb3App - completed in ${stopwatch.elapsedMilliseconds}ms.');
    initDone = true;
  }

  Future registerSignEngineListeners({ISignEngine? signEngine}) async {
    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();

    signEngine ??= _web3App.signEngine;

    // Sign engine configuration
    // Also setup the methods and chains that your wallet supports
    defaultTopicHandler(String topic, dynamic params) async {
      logger.d('defaultTopicHandler - request: $topic, params: $params');
    }

    // // Also setup the methods and chains that your wallet supports
    // final voidHandler = (dynamic params) async {
    //   logger.d('generic handler called with params: $params');
    // };

    // register for listeners
    if (requiredBlockchains.isNotEmpty) {
      for (Blockchain blockchain in requiredBlockchains) {
        for (String method in (eip155RequiredNamespace.methods + eip155OptionalNamespace.methods) ) {
          // logger.d(
          //     'registerSignEngineListeners - Registering default request handler for ${blockchain.chainId} - $method');
          signEngine.registerRequestHandler(
            chainId: blockchain.chainId,
            method: method,
            handler: defaultTopicHandler,
          );
        }
      }
    }

    // TODO - Add platform checks as this assumes mobile only when fixing metadata
    signEngine.onSessionConnect.subscribe((SessionConnect? sessionConnect) async {
      _sessionTopic = sessionConnect?.session.topic ?? 'none';
      String pairingTopic = sessionConnect?.session.pairingTopic ?? 'none';

      if (sessionConnect != null) {
        // logger.d('signEngine.onSessionConnect - $connectedSessionCount active sessions.');
        serviceStats.setSessionConnected(_sessionTopic);

        logger.i('onSessionConnect -'
            '\n****Wallet Responded with New Session ****'
            '\n* ${'name'.padLeft(15)}: ${sessionConnect.session.peer.metadata.name} '
            '- ${sessionConnect.session.peer.metadata.description}'
            '\n* ${'accounts'.padLeft(15)}: ${printNameSpacesAccounts(sessionConnect.session.namespaces)},'
            '\n* ${'methods'.padLeft(15)}: ${printNameSpacesMethods(sessionConnect.session.namespaces)},'
            '\n* ${'events'.padLeft(15)}: ${printNameSpacesEvents(sessionConnect.session.namespaces)},'
            '\n* ${'expires'.padLeft(15)}: ${printDurationFromNow(sessionConnect.session.expiry)}'
            '\n* ${'session topic'.padLeft(15)}: ${sessionConnect.session.topic},'
            '\n* ${'pairing topic'.padLeft(15)}: ${sessionConnect.session.pairingTopic},'
            '\n* ${'our redirect'.padLeft(15)}: ${sessionConnect.session.self.metadata.redirect?.native},'
            '\n* ${'properties'.padLeft(15)}: ${sessionConnect.session.sessionProperties},'
            '\n* ${'icons'.padLeft(15)}: ${sessionConnect.session.peer.metadata.icons},');

        // Compatibility checking and remediation.  Assumes the most recent connect
        // comes from the last wallet selected by the user in selectedRegistryListing.
        PairingMetadata? originalPairingMetadata = _web3App.signEngine.sessions.get(_sessionTopic)?.peer.metadata;
        PairingMetadata? updatedPairingMetadata = originalPairingMetadata;
        if (sessionConnect.session.peer.metadata.icons.isEmpty && selectedRegistryListing.image_url?.md != null) {
          logger.w('onSessionConnect - Peer metadata has no icons - using icon from Registry listing');
          updatedPairingMetadata = updatedPairingMetadata?.copyWith(
            icons: [selectedRegistryListing.image_url?.md ?? ''],
          );
        }
        if (sessionConnect.session.peer.metadata.redirect?.native == null) {
          logger.w(
              'onSessionConnect - Peer native redirect is null. Using native: ${selectedRegistryListing.mobile?.native} universal: $selectedRegistryListing.mobile?.universal');
          updatedPairingMetadata = updatedPairingMetadata?.copyWith(
            redirect: Redirect(
                native: selectedRegistryListing.mobile?.native, universal: selectedRegistryListing.mobile?.universal),
          );
        }
        if (originalPairingMetadata != updatedPairingMetadata && updatedPairingMetadata != null) {
          logger.i('onSessionConnect -*** updatedPairingMetadata: $updatedPairingMetadata');
          await _web3App.core.pairing.updateMetadata(topic: pairingTopic, metadata: updatedPairingMetadata);
          // logger.i('onSessionConnect -*** after: ${_web3App.signEngine.sessions.get(_sessionTopic)?.peer.metadata}');
        }

        /// Reload account cache
        initBlockchainAccounts();
        return;
      }
      logger.d('signEngine.onSessionConnect - null data passed');
    });

    signEngine.onSessionEvent.subscribe((SessionEvent? sessionEvent) {
      logger.d('signEngine.onSessionEvent  - $sessionEvent');
      return;
    });

    signEngine.onSessionUpdate.subscribe((SessionUpdate? sessionUpdate) {
      logger.d('signEngine.onSessionUpdate  - $sessionUpdate');

      // Re-read the list of available accounts.  (Will lose cached domain name info.);
      initBlockchainAccounts();

      // May be used to detect a session update that occurs within another call.
      lastSessionUpdateId = sessionUpdate?.id ?? 0;
      return;
    });

    // Notification when a session ping request has been received
    signEngine.onSessionPing.subscribe((SessionPing? sessionPing) {
      if (sessionPing != null) {
        logger.d('signEngine.onSessionPing - $sessionPing');
        serviceStats.incrementPingRXCount(sessionPing.topic);
        // Library verifies the session exists and has not expired
        // Library automatically replies, no action required.
        return;
      }
      logger.e('signEngine.onSessionPing - null data passed');
    });

    signEngine.onSessionExpire.subscribe((SessionExpire? sessionExpire) {
      logger.w('signEngine.onSessionExpire - $sessionExpire');
      initBlockchainAccounts();
      return;
    });

    signEngine.onSessionExtend.subscribe((SessionExtend? sessionExtend) {
      logger.d('signEngine.onSessionExtend  - $sessionExtend');
      return;
    });

    signEngine.onSessionDelete.subscribe((SessionDelete? sessionDelete) {
      logger.d('signEngine.onSessionDelete - $sessionDelete');
      if (sessionDelete?.topic != null) {
        serviceStats.setSessionDeleted(sessionDelete!.topic);
      }
      initBlockchainAccounts();
      return;
    });

    logger.d('registerSignEngineListeners - completed in ${stopwatch.elapsedMilliseconds}ms.');
  }

  /// Rebuild list of blockchain accounts from current connected peers
  initBlockchainAccounts() {
    ISignEngine signEngine = _web3App.signEngine;

    _blockchainAccounts = [];
    for (SessionData? sessionData in signEngine.sessions.getAll()) {
      if (sessionData == null) {
        logger.e('initBlockchainAccounts - No active sessions');
        continue;
      }
      _blockchainAccounts += namespacesToBlockchainAccounts(sessionData.namespaces);
    }
  }

  /// For all topics, delete the connection
  Future deleteAllSignSessions() async {
    logger.d('deleteAllSessions - ');
    List<SessionData> allSessions = _web3App.signEngine.sessions.getAll();
    for (SessionData session in allSessions) {
      String topic = session.topic;
      logger.d('deleteAllSessions - Deleting session with topic: $topic');
      await _web3App.signEngine.sessions.delete(topic);
    }
    return;
  }

  /// Logger for Sign Client Feature Set
  void showSignClientInfo() {
    if (!initDone) {
      logger.d('Sign Client not configured. Need to run init first.');
      return;
    }
    logger.d('=Sign Client Info - (${_web3App.signEngine.core.protocol} v${_web3App.signEngine.core.version})=');

    List<SessionData> sessions = _web3App.signEngine.sessions.getAll();
    logger.d('==Sign Client Sessions (${sessions.length})==');
    if (sessions.isNotEmpty) {
      for (SessionData sessionData in sessions) {
        showSessionData(sessionData);
        //logger.d(
        //    'controller: ${sessionData.controller} ack: ${sessionData.acknowledged}, expires: ${sessionData.expiry}s topic: ${sessionData.topic} peer name: ${sessionData.peer.metadata.name}');
      }
    } else {
      logger.d('No sessions.');
    }

    List<ProposalData> proposals = _web3App.signEngine.proposals.getAll();
    logger.d('==Sign Client Proposals (${proposals.length})==');
    if (proposals.isNotEmpty) {
      for (ProposalData proposalData in proposals) {
        String relays = '';
        for (Relay relay in proposalData.relays) {
          relays += (relays.isEmpty) ? relay.prettyPrint : ',${relay.prettyPrint}';
        }
        Duration duration = DateTime.fromMillisecondsSinceEpoch(proposalData.expiry * 1000).difference(DateTime.now());

        if (duration.isNegative) {
          logger.d('<expired> topic: ${proposalData.pairingTopic} id: ${proposalData.id}, relays: [ $relays ]');
        } else {
          logger.d(
              '${printDurationFromNow(proposalData.expiry)} topic: ${proposalData.pairingTopic} - required: ${proposalData.requiredNamespaces}, optional: ${proposalData.optionalNamespaces}, session properties: ${proposalData.sessionProperties}, id: ${proposalData.id}, relays: [ $relays ]');
        }
      }
    } else {
      logger.d('No proposals.');
    }
    // logger.d('==Sign Client Stats==');
    // logger.d(serviceStats);
  }

  void showAuthClientInfo() {
    if (!initDone) {
      logger.e('Auth Client not configured.  Need to run init first.');
      return;
    }

    logger.d('=Auth Client Info - (${_authClient.protocol} v${_authClient.version})=');

    List<PendingAuthRequest> pendingAuthRequests = _authClient.authRequests.getAll();
    logger.d('==Auth Client Requests (${pendingAuthRequests.length})==');
    if (pendingAuthRequests.isNotEmpty) {
      for (PendingAuthRequest pendingAuthRequest in pendingAuthRequests) {
        logger.d(
            '${pendingAuthRequest.id}: ${pendingAuthRequest.metadata.metadata.name}, expires ${pendingAuthRequest.cacaoPayload.exp}');
        //logger.d(
        //    'controller: ${sessionData.controller} ack: ${sessionData.acknowledged}, expires: ${sessionData.expiry}s topic: ${sessionData.topic} peer name: ${sessionData.peer.metadata.name}');
      }
    } else {
      logger.d('No requests.');
    }
  }

  /// A number of users are having compatibility issues, this does some basic checking
  bool sanityCheckWalletCompatible(
      ExplorerRegistryListing selectedRegistryListing, RequiredNamespace eip155RequiredNamespace) {
    Set<Blockchain> walletChains = Blockchain.fromChainIds(selectedRegistryListing.chains);
    Set<Blockchain> requiredChains = Blockchain.fromChainIds(eip155RequiredNamespace.chains);
    if (!walletChains.containsAll(requiredChains)) {
      logger.e(
          'Chain required that is not supported!  Expect wallet to reject session.  Required chains: ${eip155RequiredNamespace.chains}, Wallet supports chains: ${selectedRegistryListing.chains}');
    }

    logger.d(
        'Required methods: ${eip155RequiredNamespace.methods}, \nRequired events: ${eip155RequiredNamespace.events}');
    List<String> standards = selectedRegistryListing.standardsList;
    logger.d('Wallet supports standard(s):  ${standards.join(',')}');
    return true;
  }

  ///
  /// https://eips.ethereum.org/EIPS/eip-4361
  Future signInWeb3({required BuildContext context, bool androidUseOsPicker = true}) async {
    DateTime notBefore = DateTime.now();
    // Truncate time for better UX
    notBefore = DateTime.fromMillisecondsSinceEpoch(notBefore.millisecondsSinceEpoch -
        notBefore.millisecondsSinceEpoch % const Duration(seconds: 1).inMilliseconds);
    DateTime expires = notBefore.add(const Duration(days: 7));

    // You can also request authentication
    final AuthRequestResponse authReq = await _web3App.requestAuth(
      params: AuthRequestParams(
        aud: 'http://localhost:3000/login',
        domain: 'localhost:3000',
        chainId: 'eip155:1',
        statement:
            'By signing in you are demonstrating to $kAppName you are the owner of the account selected. No account connection, access, or permissions in any form is granted by this EIP-4361 request.'
            '\nThis request might also indicate you agree to a terms of service, or just agree to be a good person etc.'
            '\nSo you might want to review at least the contents of this message.',
        nbf: notBefore.toIso8601String(),
        // Not before "now"
        exp: expires.toIso8601String(),
        resources: ['https://eips.ethereum.org/EIPS/eip-4361'], // kb articles etc
      ),
      //pairingTopic: resp.pairingTopic,
    );

    if (authReq.uri == null) {
      logger.e('signInWeb3: No URI to launch');
      return;
    } else {
      Uri launchUri = authReq.uri!;
      logger.i('Launching $launchUri');
      launchUrl(launchUri);
    }

    // Await the auth response using the provided completer
    final AuthResponse authResponse = await authReq.completer.future;
    if (authResponse.result != null) {
      // Having a result means you have the signature and it is verified.

      // Retrieve the wallet address from a successful response
      final walletAddress = AddressUtils.getDidAddress(authResponse.result!.p.iss);
      logger.d('signInWeb3 - walletAddress: $walletAddress');
    } else {
      // Otherwise, you might have gotten a WalletConnectError if there was un issue verifying the signature.
      final WalletConnectError? error = authResponse.error;
      logger.e(
          'signInWeb3 - WalletConnect error: ${error?.code ?? 'null code'} - ${error?.message ?? 'null message'}.  authResponse: $authResponse.}');
      // Of a JsonRpcError if something went wrong when signing with the wallet.
      //final JsonRpcError? error = authResponse.jsonRpcError;
    }
  }

  Future<String?> createWalletConnectSession({required BuildContext context, bool androidUseOsPicker = true}) async {
    if (!initDone) {
      logger.e('createWalletConnectSession: Initialization has not completed.');
      return null;
    }

    Stopwatch stopwatch = Stopwatch();

    logger.d(
        'createWalletConnectSession - Create SignClient connection request for $supportedBlockchainsToCAIP2String. isRelayUp: $isRelayUp, isSignClientUp: $isSignClientUp');
    stopwatch.start();

    // No use opening a wallet, if the WalletConnect Relay isn't talking to us yet.
    if (!isRelayUp) {
      logger.e('createWalletConnectSession - Relay is down, unable to create a new session.  Attempting to reconnect.');
      relayClientConnect();
      return 'Error: Relay is down, unable to create a new session.';
    }
    logger.d('createWalletConnectSession - requiredNamespace: $eip155RequiredNamespace');

    ConnectResponse resp = await _web3App.signEngine.connect(
      requiredNamespaces: {
        'eip155': eip155RequiredNamespace,
      },
      optionalNamespaces: {
        'eip155' : eip155OptionalNamespace,
      }
    );
    final Uri? paringUri = resp.uri;

    if (paringUri == null) {
      logger.e('createWalletConnectSession -No URI returned from connect method.');
      return null;
    }

    // What is in the URI? WalletConnect URI Format is EIP-1328
    // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1328.md
    final String decodedUriString = Uri.decodeFull(paringUri.toString());
    logger.d('createWalletConnectSession - paringUri: $decodedUriString');

    final String pairingTopic = resp.pairingTopic;
    // We need to keep track of the connected wallets so we know how to contact them again etc.
    walletPairingInfoMap[pairingTopic] = WalletPairingInfo(pairingTopic: pairingTopic, wcUri: paringUri);
    savePairingInfo();

    // wait for future on another thread and close the showModalBottomSheet if open
    // If user uses QR code - this is needed.  TODO: If user rejects there is no future.
    resp.session.future.then((value) {
      // Only do something if we have a valid context still
      if (context.mounted) {
        // Multi-page apps - probably need to verify the exact modal page is open, but using simple test for now.
        if (Navigator.canPop(context)) {
          logger.d('Session returned and modal ');
          // Future: Pop until home route
          Navigator.pop(context, 'QR Code scanned');
        }
      }
    });

    /// Skip ahead if we are Android and don't want to use the SimpleWeb3Modal
    if (!(targetPlatform == TargetPlatform.android && androidUseOsPicker)) {
      // This is an alternate means to get a return value from the dialog
      var showModalResult = await showModalBottomSheet(
        context: context,
        builder: (bottomSheetConext) {
          return StatefulBuilder(builder: (stateContext, setState) {
            return SimpleWeb3Modal(
                //context: stateContext,
                walletList: explorerRegistryListings,
                showQrCode: kIsWeb, // Default to QR on Chrome
                urlString: paringUri.toString());
          });
        },
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
      );
      // Errors are passed as Strings, and if we forget to pass a string its probably null
      if (showModalResult.runtimeType == String || showModalResult == null) {
        logger.w('createWalletConnectSession - $showModalResult.  Nothing to launch.');
        return null;
      }
      // Otherwise we expect the type to be a RegistryListing
      try {
        selectedRegistryListing = showModalResult;
        walletPairingInfoMap[pairingTopic]?.registryListing = selectedRegistryListing;
        savePairingInfo();
      } on Exception catch (e, s) {
        logger.w('createWalletConnectSession - $e showModalResult returned an unexpected type: $showModalResult\n$s');
        return null;
      }

      sanityCheckWalletCompatible(selectedRegistryListing, eip155RequiredNamespace);

      logger.i('createWalletConnectSession - Returned from showWalletSelectionDialog with selectedRegistryListing:'
          '\n****User Selected wallet from modal ****'
          '\n* ${'name'.padLeft(16)}: ${selectedRegistryListing.name} - ${selectedRegistryListing.description},'
          '\n* ${'mobile native'.padLeft(16)}: ${selectedRegistryListing.mobile?.native},'
          '\n* ${'mobile universal'.padLeft(16)}: ${selectedRegistryListing.mobile?.universal},'
          // '\n* ${'desktop native'.padLeft(16)}: ${selectedRegistryListing.desktop?.native}'
          // '\n* ${'desktop universal'.padLeft(16)}: ${selectedRegistryListing.desktop?.universal},'
          '\n* ${'app_type'.padLeft(16)}: ${selectedRegistryListing.appType},'
          '\n* ${'chains'.padLeft(16)}: ${selectedRegistryListing.chains?.join(',')}'
          '\n* ${'versions'.padLeft(16)}: ${selectedRegistryListing.versions?.join(',')}'
          '\n* ${'sdks'.padLeft(16)}: ${selectedRegistryListing.sdks?.join(',')}'
          '\n* ${'standards'.padLeft(16)}: ${selectedRegistryListing.standardsList.join(',')}'
          '\n* ${'image_url sm'.padLeft(16)}: ${selectedRegistryListing.image_url?.sm}');
    }

    try {
      /// Continue all platforms and launch the wallet to initiate pairing
      bool result = await explorerRegistryService.launchAppWithParingUri(
          platform: targetPlatform,
          listing: selectedRegistryListing,
          paringUri: paringUri,
          androidUseOsPicker: androidUseOsPicker);

      logger.d('createWalletConnectSession - launchAppWithParingUri returned $result');
      // UI feedback if error occurs
      // await showDialog<String>(
      //   context: context,
      //   builder: (BuildContext context) => DemoAlertDialog(
      //     icon: FontAwesomeIcons.triangleExclamation,
      //     text: 'Unable to open ${appSettings.selectedWalletApp.name}.',
      //     actions: <Widget>[
      //       ElevatedButton(
      //         onPressed: () async {
      //           Navigator.pop(context, 'Done');
      //         },
      //         child: const Text('DONE'),
      //       ),
      //     ],
      //   ),
      // );
      logger.d('Awaiting session completer before returning.');
      await resp.session.future;
    } on JsonRpcError catch (e, s) {
      // Trustwallet passed JsonRpcError: 4001 - Requested chains are not supported
      // Errors I expected https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-25.md#failure-states
      String? errorMessage = e.message;
      if (errorMessage != null) {
        if (errorMessage.contains('chains are not supported')) {
          logger.e(
              'Error from wallet: ${e.code} - ${e.message} Requested chains: ${eip155RequiredNamespace.chains ?? 'None'}');
        }
        if (errorMessage.contains('Unsupported methods')) {
          logger.e('Error from wallet: ${e.code} - ${e.message} Requested methods: ${eip155RequiredNamespace.methods}');
        }
        if (errorMessage.contains('Rejected by user')) {
          logger
              .e('Error from wallet: ${e.code} - ${e.message}. Bummer rejecting yourself like that. Blame the wallet.');
        } else {
          logger.e('createWalletConnectSession JsonRpcError: ${e.code} - ${e.message}\n$s');
        }
      } else {
        logger.e('createWalletConnectSession JsonRpcError with null message: ${e.code} - ${e.message}\n$s');
      }
    } catch (e, s) {
      logger.e('createWalletConnectSession Exception: $e\n$s');
    }

    stopwatch.stop();
    logger.d(
        'createWalletConnectSession - Connection with wallet took ${(stopwatch.elapsedMicroseconds / 1000).toStringAsFixed(2)} seconds.');
    // Log after state
    // showRelayInfo();
    // showSignClientInfo();
    return "Sessions?";
  }

  /// https://eips.ethereum.org/EIPS/eip-3326
  /// Metamask bug https://github.com/MetaMask/metamask-mobile/issues/6655#issue-1767350452
  Future walletSwitchEthereumBlockchain({required Blockchain blockchain, required String sessionTopic}) async {
    String ethereumChainId = '0x${blockchain.eip155ChainId.toRadixString(16)}';
    logger.d('walletSwitchBlockchain to $blockchain with chainId $ethereumChainId for session topic: $sessionTopic');
    try {
      var result = await _web3App.request(
        topic: sessionTopic,
        chainId: blockchain.chainId, //  NOT  blockchain.eip155ChainId?
        request: SessionRequestParams(
          method: 'wallet_switchEthereumChain',
          params: [
            {'chainId': ethereumChainId}
          ],
        ),
      );
      logger.d('walletSwitchEthereumBlockchain - result: $result');
    } on WalletConnectError catch (error) {
      logger.e(
          'Wallet refused request. Did you include the method wallet_switchEthereumChain in the eip155RequiredNamespace or eip155OptionalNamespace?  \nCode: ${error.code} - ${error.message}');
    }
  }
}
