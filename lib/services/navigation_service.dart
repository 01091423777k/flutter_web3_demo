import 'package:flutter/material.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:flutter_web3_demo/widgets/demo_alert_dialog.dart';
import 'package:logger/logger.dart';

// Maintain a global reference to the current navigation key
class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  static final NavigationService _singleton = NavigationService._internal();

  factory NavigationService() {
    return _singleton;
  }

  NavigationService._internal();

  final logger = Logger(printer: SimpleLogPrinter('NavigationService'));

  /// Show a dialog, without needing to pass a BuildContext
  ///
  /// Useful when an event occurs, but you don't necessarily know what page the user is on
  Future<String?> showDemoAlertDialog({
    final IconData? icon,
    final Color? iconColor = Colors.red,
    required final String text,
    required final List<Widget> actions,
  }) async {
    logger.d('showDemoAlertDialog called with text: $text');
    return showDialog<String>(
        context: navigatorKey.currentContext!,
        builder: (BuildContext context) => DemoAlertDialog(
              text: text,
              actions: actions,
              icon: icon,
            ));
  }
}
