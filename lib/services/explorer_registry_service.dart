import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_web3_demo/model/blockchain.dart';
import 'package:flutter_web3_demo/model/env_settings.dart';
import 'package:flutter_web3_demo/model/explorer_registry_listing.dart';
import 'package:flutter_web3_demo/services/log_printer.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_flutter_v2/apis/core/pairing/utils/pairing_models.dart';

/// Wallet Connect Explorer Registry API Documentation: https://docs.walletconnect.com/2.0/cloud/explorer
class ExplorerRegistryService {
  final logger = Logger(printer: SimpleLogPrinter('ExplorerRegSvc '));

  // Wallet Connect Project ID from https://cloud.walletconnect.com passed from the environment. See README
  static const String _walletConnectProjectId = EnvSettings.walletConnectProjectId;
  static final ExplorerRegistryService _singleton = ExplorerRegistryService._internal();

  factory ExplorerRegistryService() {
    return _singleton;
  }

  ExplorerRegistryService._internal();

  String listToCommaSeparatedString(List<String> listOfStrings) {
    String commaSeparatedString = '';
    for (String value in listOfStrings) {
      // Comma separate after first entry
      commaSeparatedString = (commaSeparatedString == '' ? '' : ',') + value;
    }
    return commaSeparatedString;
  }

  ///  Query the WalletConnect Explorer Registry for compatible wallets
  ///
  /// [limit]  Return first X entries that meet the criteria
  /// [supportedChains]  Comma separated list of chains in CAIPs format, any matches are returned (not ALL)
  /// [walletConnectProjectId] required either directly or from environment
  /// [alwaysIncludeWalletIds] Only return wallets that match the Ids provided (Ignores supported chains etc.)
  /// [alwaysExcludeWalletIds] Omit wallets that match the Id provided
  ///
  Future<List<ExplorerRegistryListing>> readWalletRegistry({
    int limit = 30,
    String supportedCAIP2Chains = 'eip155:1,eip155:137', // Ethereum and Polygon Mainnets
    String walletConnectProjectId = _walletConnectProjectId,
    required TargetPlatform targetPlatform,
    List<String> alwaysIncludeWalletIds = const [],
    List<String> alwaysExcludeWalletIds = const [],
  }) async {
    List<ExplorerRegistryListing> listings = [];
    int numResponseListings = 0;

    if (walletConnectProjectId.isEmpty) {
      logger.e('readWalletRegistry - walletConnectProjectId is empty, but required.  See README for more info.');
      return listings;
    }

    var client = http.Client();

    String platform;
    switch (targetPlatform) {
      case TargetPlatform.iOS:
        platform = 'ios';
        break;
      case TargetPlatform.android:
        platform = 'android';
        break;
      case TargetPlatform.fuchsia:
        platform = 'browser';
        break;
      case TargetPlatform.macOS:
        platform = 'mac';
        break;
      case TargetPlatform.windows:
        platform = 'windows';
        break;
      case TargetPlatform.linux:
        platform = 'linux';
        break;
      default:
        platform = ''; // Do not use platform
    }

    Map<String, dynamic> queryParameters = {};

    if (alwaysIncludeWalletIds.isNotEmpty) {
      logger.d(
          'readWalletRegistry - Requesting WalletConnect Registry for $limit wallet(s) for $platform with Ids: $alwaysIncludeWalletIds.');
      queryParameters = {
        'entries': '${alwaysIncludeWalletIds.length}',
        'page': '1',
        'projectId': walletConnectProjectId, // This is unique to your project
        'ids': alwaysIncludeWalletIds.join(','), //New for V2 - any id will match (other criteria are ignored)
        'platforms': (kIsWeb) ? 'browser' : platform, // New for V2 - any platform match will return (not all)
      };
    } else {
      logger.d(
          'readWalletRegistry - Requesting WalletConnect Registry for first $limit wallets for $platform that support any of the following chains: $supportedCAIP2Chains, excluding ${alwaysExcludeWalletIds.length} entries marked alwaysExclude.');
      queryParameters = {
        'entries': '$limit', // This example doesn't page
        'page': '1', // TODO: This example app doesn't page
        'projectId': walletConnectProjectId, // This is unique to your project
        // TODO: Check the filters for your project needs - https://docs.walletconnect.com/2.0/cloud/explorer#listings
        'sdks': 'sign_v2', //,auth_v2', // WalletConnect V2 - keep this
        'chains': supportedCAIP2Chains, // New for V2 - any chain match will return (not  all)
        'platforms': (kIsWeb) ? 'browser' : platform, // New for V2 - any platform match will return (not all)
      };
    }

    http.Response? response;
    try {
      // logger.i('queryParameters: $queryParameters');
      response = await client.get(
        Uri.https(
          'explorer-api.walletconnect.com', // Formerly 'registry.walletconnect.com',
          'v3/wallets', // formerly 'api/v1/wallets',
          queryParameters,
        ),
        headers: {
          'Content-Type': 'application/json',
        },
      ).timeout(const Duration(seconds: 10)); // NOTE: 5 timeout was too aggressive
    } on http.ClientException catch (e) {
      if (e.message.contains('XMLHttpRequest error')) {
        logger.e(
            'WalletConnect Registry Server is not properly configured to accept requests from browser based applications.\nComment on - https://discord.com/channels/492410046307631105/1073721960288227410');
      } else {
        logger.e('Unexpected HTTP Client exception ${e.message}, ${e.uri}');
      }
      return listings;
    } catch (e) {
      logger.e('readWalletRegistry - Unexpected protocol error: $e');
      return listings;
    }

    if (response.statusCode == 200) {
      //logger.i(response.body);
      var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map<String, dynamic>?;

      if (decodedResponse != null && decodedResponse['listings'] != null) {
        for (Map<String, dynamic> entry in decodedResponse['listings'].values) {
          // Debugging that final json
          ExplorerRegistryListing listing = const ExplorerRegistryListing();
          try {
            listing = ExplorerRegistryListing.fromJson(entry);
          } on Exception catch (e) {
            logger.e('Unable to decode ExplorerRegistryListing. Error: $e');
            log(jsonEncode(entry));
            continue;
          }
          // Explorer API doesn't seem to filter as I had hoped.
          //
          // TODO: Double check your requirements
          numResponseListings++;
          bool acceptListing = true;
          String issues = "";

          if (alwaysIncludeWalletIds.contains(listing.id)) {
            // Accept Listing as there is no need to check other acceptance criteria
            listings.add(listing);
            logger.d(
                'readWalletRegistry - Processing ${listing.appType} ${listing.name} - supported chains:${listing.chains}, alwaysIncludeWallet: true');
            continue;
          }

          if (alwaysExcludeWalletIds.contains(listing.id)) {
            acceptListing = false;
            issues += 'app ID on the alwaysExclude List';
          }

          if (!(listing.appType == 'wallet' || listing.appType == 'hybrid')) {
            acceptListing = false;
            issues += 'app_type ${listing.appType} is not a wallet/hybrid, ';
          }

          if (listing.chains?.isEmpty ?? true) {
            acceptListing = false;
            issues += 'chain list is empty or null, ';
          } else {
            // There are entries
            List<String> requestedChains = supportedCAIP2Chains.split(',');
            for (String chain in requestedChains) {
              if (!listing.chains.toString().contains(chain)) {
                acceptListing = false;
                issues += 'missing chain $chain, ';
              }
            }
          }
          if (acceptListing) {
            // Accept Listing
            listings.add(listing);
            //logger.d(
            //    'readWalletRegistry - Processing ${listing.app_type} id: ${listing.id} name: ${listing.name} supported chains:${listing.chains}');
          } else {
            logger.w(
                'readWalletRegistry - Skipping ${listing.appType} id: ${listing.id} name: ${listing.name} supported chains:${listing.chains} - issues: $issues');
          }
        }
      }
      logger
          .d('readWalletRegistry - Found ${listings.length} compatible listings out of $numResponseListings returned.');
      return listings;
    } else {
      logger.e('readWalletRegistry - Unexpected server error: ${response.statusCode}: ${response.reasonPhrase}.');
    }
    client.close();

    return listings;
  }

  /// Direct the user to the appropriate app store listing for the wallet they have chosen.
  ///
  /// Used as a last resort, if the wallet is not installed on the device.
  Future<bool> launchAppStoreListing(
      {required TargetPlatform platform, required ExplorerRegistryListing registryListing}) async {
    logger.d('launchAppStoreListing - ${registryListing.name}:');

    bool result = false;
    String appStoreUrl;

    // Find the platform specific link if available
    switch (platform) {
      case TargetPlatform.iOS:
        //   "ios": "https://apps.apple.com/app/rainbow-ethereum-wallet/id1457119021",
        appStoreUrl = registryListing.app?.ios ?? '';
        break;
      case TargetPlatform.android:
        //   "android": "https://play.google.com/store/apps/details?id=me.rainbow",
        appStoreUrl = registryListing.app?.android ?? '';
        break;
      default:
        logger.e('launchAppStoreListing - $platform unimplemented. Falling back to homepage.');
        appStoreUrl = registryListing.homepage ?? '';
        break;
    }

    // Last resort - open the app home page.
    if (appStoreUrl.isEmpty) {
      logger.w('launchAppStoreListing - empty/null value for the app store listing. Falling back to homepage');
      appStoreUrl = registryListing.homepage ?? '';
      // Check if homepage is also blank
      if (appStoreUrl.isEmpty) {
        logger.e('launchAppStoreListing -  empty/null value for the app home page. Doing nothing.');
        return false;
      }
    }

    Uri uri = Uri.parse(appStoreUrl);
    // App Stores are native, no need to try alternate LaunchModes
    try {
      logger.i('launchAppStoreListing - launchUrl: ${Uri.decodeFull(uri.toString())}');
      result = await launchUrl(uri, mode: LaunchMode.externalApplication);
    } on Exception catch (e) {
      result = false;
      logger.e(
          'launchAppStoreListing - Unexpected error opening store for ${registryListing.name} with uri ${uri.toString()} - $e');
    }
    return result;
  }

  /// Launch the WalletConnect URI to initiate Pairing using the selected Explorer Listing (wallet)
  ///
  /// [platform] Current platform
  /// [listing] Registry listing that includes the wallet deep links
  /// [pairingUri] URI generated from WalletConnect that include the session proposal
  ///
  /// NOTE: IOS may need deep links declared.  See https://pub.dev/packages/walletconnect_modal_flutter#ios-setup
  Future<bool> launchAppWithParingUri({
    required TargetPlatform platform,
    required ExplorerRegistryListing listing,
    required Uri paringUri,
    androidUseOsPicker = true,
  }) async {
    bool result = false;

    // Related: https://docs.walletconnect.com/2.0/web3modal/faq#how-do-i-add-a-native-wallet-to-web3modal
    // Launcher Strategy -
    // 1. Try calling platform preferred method
    // 2. Try calling platform fallback method
    // 3. Direct the user to the appstore
    // 4. Direct the user to the application home page
    // 5. Do nothing.

    // Save a copy for fallback.
    Uri originalUri = paringUri;

    // The URI contains square brackets that need to be encoded
    String encodedWcUrl = Uri.encodeComponent(paringUri.toString());

    String urlSource = 'unknown';
    // Customize the URI to the platform preferred linking method
    switch (platform) {
      case TargetPlatform.iOS:
        String uriBase = listing.mobile?.universal ?? '';
        if (uriBase.isNotEmpty) {
          paringUri = Uri.parse('$uriBase/wc?uri=$encodedWcUrl');
          urlSource = 'mobile universal';
          break;
        }
        logger.w(
            'launchApp - universal link failed - empty/null value in listing.  Consider hiding such entries on IOS?');

        // IOS doesn't support  to try native links

        uriBase = listing.mobile?.native ?? '';
        if (uriBase.isNotEmpty) {
          // Some registry listings do not have a trailing //, add if missing
          String trailingCharacter = uriBase.substring(uriBase.length - 1);
          if (!(trailingCharacter == '/')) {
            logger.w('launchApp - native link does not end with  / *** compatibility ***  adding //');
            paringUri = Uri.parse('$uriBase//wc?uri=$encodedWcUrl');
          } else {
            paringUri = Uri.parse('${uriBase}wc?uri=$encodedWcUrl');
          }
          urlSource = 'mobile native';
          break;
        }

        break;
      case TargetPlatform.android:
        String uriBase;

        /// Android opt-out of Modal since Native/Universal Links appear unreliable
        if (androidUseOsPicker) {
          // If we didn't use the Web3 Modal - there is no native or universal link to launch
          // urlSource = 'WalletConnect URI';
          paringUri = originalUri;
          urlSource = 'originalUri';
          break;
        } else {
          uriBase = listing.mobile?.native ?? '';
          if (uriBase.isNotEmpty) {
            // Some registry listings do not have a trailing //, add if missing
            String trailingCharacter = uriBase.substring(uriBase.length - 1);
            if (!(trailingCharacter == '/')) {
              logger.w('launchApp - native link does not end with  / *** compatibility ***  adding //');
              paringUri = Uri.parse('$uriBase//wc?uri=$encodedWcUrl');
            } else {
              paringUri = Uri.parse('${uriBase}wc?uri=$encodedWcUrl');
            }
            urlSource = 'mobile native';
            break;
          }
          logger.w('launchApp - native link failed -  empty/null value in listing.');

          uriBase = listing.mobile?.universal ?? '';
          if (uriBase.isNotEmpty) {
            paringUri = Uri.parse('$uriBase/wc?uri=$encodedWcUrl');
            urlSource = 'mobile universal';
            break;
          }
          logger.w('launchApp - universal link failed - empty/null value in listing.');
          urlSource = 'WalletConnect URI';
          paringUri = originalUri;
          break;
        }
      case TargetPlatform.fuchsia:
        String uriBase = listing.app?.browser ?? '';
        if (uriBase.isNotEmpty) {
          paringUri = Uri.parse('$uriBase/wc?uri=$encodedWcUrl');
          urlSource = 'app browser';
          break;
        }
        logger.w('launchApp - browser link failed - empty/null value in listing.');
        break;
      default:
        logger.w('launchApp - Unimplemented platform $platform.  Unable to launch a preferred method.');
    }

    // 1. Platform preferred method external application (non-browser)
    try {
      logger.i(
          'launchApp - 1. $urlSource using LaunchMode.externalApplication \n${Uri.decodeFull(paringUri.toString())}');
      result = await launchUrl(paringUri, mode: LaunchMode.externalApplication);
    } on PlatformException catch (e) {
      // Android Intent Fails - We will try with universal link below in 1.a
      if (e.code == 'ACTIVITY_NOT_FOUND') {
        logger.w(
            'launchApp - 1. $urlSource failed - Android Intent returned ACTIVITY_NOT_FOUND (app missing support it or app not installed.)');
      } else {
        logger.e(
            'launchApp - 1. $urlSource failed - Unexpected PlatformException error opening ${listing.name}: ${e.message}, code: ${e.code}, details: ${e.details}');
      }
    } on Exception catch (e) {
      logger.e('launchApp - 1. $urlSource failed - unexpected error e: $e');
    }

    // 1.a  Android universal attempt
    if (result == false && platform == TargetPlatform.android) {
      // We either had a blank native link or the platform intent failed.  Trying the universal link
      String uriBase = listing.mobile?.universal ?? '';
      if (uriBase.isEmpty) {
        logger.w('launchApp - 1.a mobile universal failed - empty/null value in listing.');
      } else {
        paringUri = Uri.parse('$uriBase/wc?uri=$paringUri');
        urlSource = 'mobile universal';
        try {
          logger.i(
              'launchApp - 1.a $urlSource using LaunchMode.externalApplication with mobile universal \n${Uri.decodeFull(paringUri.toString())}');
          result = await launchUrl(paringUri, mode: LaunchMode.externalApplication);
        } on PlatformException catch (e) {
          logger.e('launchApp - 1.a $urlSource failed - PlatformException error opening ${listing.name}: $e');
        } on Exception catch (e) {
          logger.e('launchApp - 1.a $urlSource failed - Unexpected error e: $e');
        }
      }
    }

    // // can result in a duplicate call on IOS when there is no universal link
    // // Launch universal link with alternate launching method -
    // if (result == false) {
    //   // launch alternative method
    //   logger.w(
    //       'launchApp - failed using external application. Fallback to LaunchMode.platformDefault \n${paringUri.toString()}');
    //   try {
    //     result = await launchUrl(paringUri, mode: LaunchMode.platformDefault);
    //   } on Exception catch (e) {
    //     result = false;
    //     logger.e('launchApp - Unexpected error e: $e');
    //   }
    // }

    // 2  Try asking the OS for wallets that recognize the wc: url.
    if (result == false && await canLaunchUrl(originalUri)) {
      urlSource = 'WalletConnect URI';
      logger.d(
          'launchApp - 2. OS confirms we have apps that can launch the wc URI, but app selection is controlled by the OS.');
      logger.i(
          'launchApp - 2. $urlSource using LaunchMode.externalApplication with mobile universal \n${Uri.decodeFull(originalUri.toString())}');
      try {
        result = await launchUrl(originalUri, mode: LaunchMode.externalApplication);
      } on Exception catch (e) {
        result = false;
        logger.e('launchApp - 2. $urlSource failed - Unexpected error e: $e');
      }
    }

    // 3 & 4 - Direct the user to the appstore / homepage
    if (result == false) {
      // OK try the app store
      result = await launchAppStoreListing(platform: platform, registryListing: listing);
    }
    // 5. Do nothing.
    return result;
  }

  // Assumptions:
  // * The wallet has already been used on this device (i.e. installed so no app store fallback)
  // * The wallet will be called via the platform preferred method, no fallbacks
  // * pairingInfo has redirect info --- SPOILER --- they don't --- So we may have "fixed it"
  Future<bool> launchAppWithPairingMetadata(
      {required TargetPlatform platform, PairingMetadata? pairingMetadata, String? additionalUriString}) async {
    if (pairingMetadata == null) {
      return false;
    }

    String appLaunchUrl = '';
    // Find the platform specific link if available
    switch (platform) {
      case TargetPlatform.iOS:
        appLaunchUrl = pairingMetadata.redirect?.universal ?? '';
        break;
      case TargetPlatform.android:
        appLaunchUrl = pairingMetadata.redirect?.native ?? '';
        break;
      default:
        logger.e('launchAppWithPairingMetadata - $platform universal.');
        appLaunchUrl = pairingMetadata.redirect?.universal ?? '';
        break;
    }
    if (appLaunchUrl == '') {
      logger.e(
          'PairingData does not have required redirect urls.  redirect: ${pairingMetadata.redirect}  universal:${pairingMetadata.redirect?.universal ?? 'NULL'} native: ${pairingMetadata.redirect?.native ?? 'NULL'}');
      return false;
    }

    logger.d('launchAppWithPairingMetadata using url: $appLaunchUrl');
    return true;
  }

  /// Need to extend Blockchain with an icon URI
  ///
  /// But using this for now as the URI still required the ProjectID
  /// from https://github.com/WalletConnect/web3modal/blob/V2/packages/ui/src/presets/ChainPresets.ts
  /// https://github.com/WalletConnect/web3modal/blob/V3/packages/wagmi/src/utils/presets.ts
  static const chainPresets = {
    // Ethereum
    'eip155:1': '692ed6ba-e569-459a-556a-776476829e00',
    // Arbitrum
    'eip155:42161': '600a9a04-c1b9-42ca-6785-9b4b6ff85200',
    // Avalanche
    'eip155:43114': '30c46e53-e989-45fb-4549-be3bd4eb3b00',
    // Binance Smart Chain
    'eip155:56': '93564157-2e8e-4ce7-81df-b264dbee9b00',
    // Fantom
    'eip155:250': '06b26297-fe0c-4733-5d6b-ffa5498aac00',
    // Optimism
    'eip155:10': 'ab9c186a-c52f-464b-2906-ca59d760a400',
    // Polygon
    'eip155:137': '41d04d42-da3b-4453-8506-668cc0727900',
    // Gnosis
    'eip155:100': '02b53f6a-e3d4-479e-1cb4-21178987d100',
    // EVMos
    'eip155:9001': 'f926ff41-260d-4028-635e-91913fc28e00',
    // ZkSync
    'eip155:324': 'b310f07f-4ef7-49f3-7073-2a0a39685800',
    // Filecoin
    'eip155:314': '5a73b3dd-af74-424e-cae0-0de859ee9400',
    // Iotx
    'eip155:4689': '34e68754-e536-40da-c153-6ef2e7188a00',
    // Metis,
    'eip155:1088': '3897a66d-40b9-4833-162f-a2c90531c900',
    // Moonbeam
    'eip155:1284': '161038da-44ae-4ec7-1208-0ea569454b00',
    // Moonriver
    'eip155:1285': 'f1d73bb6-5450-4e18-38f7-fb6484264a00'
  };

  // String? explorerIconUrl({required String projectId}) => explorerIconUrlSm(projectId:projectId);
  static String? explorerIconUrlSm({required Blockchain blockchain, String? projectId}) {
    // Testnets will use the mainnet icon for the currency
    String chainPreset = chainPresets[blockchain.toMainNet.chainId] ?? '';
    projectId = projectId ?? _walletConnectProjectId;

    if (chainPreset.isNotEmpty) {
      return 'https://explorer-api.walletconnect.com/v3/logo/sm/$chainPreset?projectId=$projectId';
    }
    return null;
  }

  // String? explorerIconUrl({required String projectId}) => explorerIconUrlSm(projectId:projectId);
  //
  // curl 'https://explorer-api.walletconnect.com/v3/logo/sm/692ed6ba-e569-459a-556a-776476829e00?projectId=$PROJECT_ID' --output 692ed6ba-e569-459a-556a-776476829e00.png
  static String? explorerIconAssetSm({required Blockchain blockchain, String? projectId}) {
    // Testnets will use the mainnet icon for the currency
    String chainPreset = chainPresets[blockchain.toMainNet.chainId] ?? '';
    projectId = projectId ?? _walletConnectProjectId;

    if (chainPreset.isNotEmpty) {
      return 'assets/img/$chainPreset.png';
    }
    return null;
  }
}
