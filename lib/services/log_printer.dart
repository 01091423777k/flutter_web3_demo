import 'package:flutter_web3_demo/consts.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

// Leveraged from:https://medium.com/flutter-community/a-guide-to-setting-up-better-logging-in-flutter-3db8bab2000e

class SimpleLogPrinter extends LogPrinter {
  final String className;
  SimpleLogPrinter(this.className);
  @override
  List<String> log(LogEvent event) {
    DateFormat dateFormat = DateFormat(kLogTimeStampFormat);
    String dateString = dateFormat.format(event.time);

    //var color = PrettyPrinter.levelColors[event.level];
    var emoji = PrettyPrinter.levelEmojis[event.level];
    return ['$dateString $emoji$className - ${event.message}'];
    //println(color('$emoji $className - ${event.message}'));
  }
}
